/*
Artica CC - http://artica.cc

Digital Input support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "DeviceInputDigitalPin.h"

#include "Utils.h"
#include "DebugConfig.h"


// Constructor of the device sets the pin to be used 
DeviceInputDigitalPin::DeviceInputDigitalPin( byte pin_number, bool active_high):
	VariableBool(0)
{
	this->_pin_number = pin_number;
	this->_active_high = active_high;
	this->initialize();
}


// Initialize the device and obtain the initialization state
DEVICE_INITIALIZATION DeviceInputDigitalPin::initialize()
{
	// Initializing this device means simply configuring the pin as input

	// @todo INPUT or INPUT_PULLUP?
	pinMode(this->_pin_number, INPUT_PULLUP);
	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	printName(); spf("  pin:"); sp(this->_pin_number); spf("  active_high:"); spl(this->_active_high);
	#endif
	return DEVICE_INITIALIZATION_SUCCESSFUL;
}


// Create a device based on a byte array of data. If the data is invalid
// to create the desired device, returns null
DeviceInputDigitalPin* DeviceInputDigitalPin::createDevice(byte* data, byte data_length)
{
	// Check if the data buffer has the exact amount of data to create the device
	if (!testDataLength(data_length, 4)) return NULL;

	// Read the pin number to use for this device
	byte pin = data[2];
	bool active_high = data[3];

	DeviceInputDigitalPin* device = new DeviceInputDigitalPin( pin, active_high );
	// Create the new device and add it to the devices list
	return device;
}


// Update the value with new data from the sensor
void DeviceInputDigitalPin::update()
{
	// Get the raw state from the pin
	byte value = digitalRead(this->_pin_number);

	// If the pin is set as active low we need to toggle the state
	value ^= (!this->_active_high);

	// Finally we set the new value for the variable
	this->_value = value;
}
