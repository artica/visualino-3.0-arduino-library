/*
Artica CC - http://artica.cc

Generic Gyro device type - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "DeviceGyro.h"

#include "Utils.h"

// Default constructor
DeviceGyro::DeviceGyro(ServiceProvider* service_provider)
{
	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	spf("new DeviceGyro:  "); //sp(id); spf("  ");
	#endif
	this->_service_provider = service_provider;
}

