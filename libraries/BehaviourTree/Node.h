/*
Artica CC - http://artica.cc

Generic Node type - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef ARTICA_TREE_NODE_H
#define ARTICA_TREE_NODE_H

#include "Arduino.h"

#include "Utils.h"
#include "DebugConfig.h"


// Hex codes for the different node types
#define NODE_SEQUENCE		0x00
#define NODE_PARALLEL 		0x01
#define NODE_OVERRIDE 		0x02
#define NODE_SERVICE		0x03


// Possible states for the nodes
enum NODE_STATE 
{
	NODE_STATE_FAILURE = 0,
	NODE_STATE_RUNNING = 1,
	NODE_STATE_SUCCESS = 2,
	NODE_STATE_INACTIVE = 3
};

// Set the data type used for node types
typedef byte NODE_TYPE;

// Set the data type used for node ids
typedef byte NODE_ID;


// Base node class to represent the nodes in a tree
// Each node has a unique ID and may have a parent or not
class Node
{
	public:
		// Default constructor
		Node(NODE_ID id);

		// Default destructor
		virtual ~Node();

		// Runs the the node. Is the node wasn't running before, start() is called
		// the parameter forces start() to be called even if the node was already running
		virtual NODE_STATE run();

		// Starts the node execution, initializing all the necessary variables
		virtual void start();

		// Ends the node execution with a specific termination node state
		virtual NODE_STATE stop(NODE_STATE state);


		// Prints the node class name
		virtual void printName() {spf("Node");};


		// Prints a string with the name representing a node state
		static void printStateName(NODE_STATE state);

		// Type for this node
		NODE_TYPE _type;

		// ID for this node
		NODE_ID _id;

		// Current node state
		NODE_STATE _state;

		// State of the node in the previous frame
		NODE_STATE _old_state;


		// Tests if a data length value is the exact number of bytes
		// to create a node of a specific type
		static bool testDataLength(byte data_length, byte data_necessary);
};

#endif
