/*
Artica CC - http://artica.cc

Internal tree node - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/


#ifndef ARTICA_TREE_NODE_INTERNAL_H
#define ARTICA_TREE_NODE_INTERNAL_H

#include "Arduino.h"
#include "Node.h"

#include "Utils.h"
#include "DebugConfig.h"


// Internal node of a tree (meaning it has a list of children nodes)

class NodeInternal: public Node
{ 
	public:
		// We need to specify an id and the maximum number of children
		// when initializing an internal node
		NodeInternal(byte id, byte max_children);
		// Destructor
		~NodeInternal();
		
		// Add the next child to this node
		void addChild(Node* child);
		
		// Runs the node, returning the node state
		NODE_STATE run();

		// Start running an internal node
		void start();

		// Stops the execution of all children nodes
		NODE_STATE stop(NODE_STATE state);

	protected:
		// Array of pointers to all the children nodes
		Node** _children;
		// Child currently being processed (used both to load and to run the tree)
		byte _current_child;
		// Maximum number of children this node can have
		byte _max_children;
};

#endif
