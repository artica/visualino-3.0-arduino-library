/*
Artica CC - http://artica.cc

Motor support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "DeviceOutputMotor.h"

#include "Utils.h"
#include "DebugConfig.h"


// Constructor of the device sets the pin to be used 
DeviceOutputMotor::DeviceOutputMotor( byte pin_1, byte pin_2, int initial_value ):
	VariableInt(initial_value)
{
	this->_pin_1 = pin_1;
	this->_pin_2 = pin_2;

	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	printName(); spf(" pins: 1:"); sp(pin_1); spf(" 2:"); spl(pin_2);
	#endif
	this->initialize();
}


// Initialize the device and obtain the initialization state
DEVICE_INITIALIZATION DeviceOutputMotor::initialize()
{
	// Initializing this device means simply configuring the pin as output
	pinMode(this->_pin_1, OUTPUT);
	pinMode(this->_pin_2, OUTPUT);

	return DEVICE_INITIALIZATION_SUCCESSFUL;
}


// Create a device based of a byte array of data. If the data is invalid
// to create the desired device, returns null
DeviceOutputMotor* DeviceOutputMotor::createDevice(byte* data, byte data_length)
{
	//spf("DeviceOutputMotor:"); printArray(data, data_length);

	// Check if the data buffer has the exact amount of data to create the device
	if (!testDataLength(data_length, 6)) return NULL;

	// These 2 bytes should contain the 2 pin numbers necessary to control the motor
	byte pin_1 = data[2];
	byte pin_2 = data[3];

	// @todo read que initial value here

	DeviceOutputMotor* device = new DeviceOutputMotor( pin_1, pin_2, 0 );
	// Create the new device and return it
	return device;
}


// Update the the hardware pin with the current device value
void DeviceOutputMotor::execute()
{

	// Sets the values for both pins of the motor
	byte v1, v2;
	if (this->_value < 0)
	{  
		v1 = 0;
		v2 = (byte) this->_value * -1;
	} 
	else
	{  
		v1 = (byte) this->_value;
		v2 = 0;
	}

	//spf("MOTOR:"); spl(this->_value);
 
	// Write the calculated values to the pins 	
	analogWrite(this->_pin_1, v1);
	analogWrite(this->_pin_2, v2);
}
