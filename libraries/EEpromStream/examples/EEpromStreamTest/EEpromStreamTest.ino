#include <EEPROM.h>
#include <EEpromStream.h>

EEpromStream eeprom(1024);

void setup()
{
    Serial.begin(115200);
    Serial.println(F("Starting initialization..."));

    delay(2000);

    char heya[30] = "heya!";
    char zomgzor[30] = "zomgzor!";
    long teste_long = 12345678;
    int teste_int = 12345;
    byte teste_byte = 123;
    float teste_float = 1234.56;

    eeprom.print("T");
    eeprom.write(69);
    eeprom.write(70);
    eeprom.write(71);
    eeprom.print("?Muhahahaha!");

    eeprom.print(22); eeprom.print(' ');
    eeprom.print(33); eeprom.print(' ');
    eeprom.print(44); eeprom.print("   DONE;");


    while (eeprom.available()>0)
    {
        char c = eeprom.read();
        Serial.print(c);

        if (c == ';') 
        {
            eeprom.flush();
            Serial.println();
        }
    }

    Serial.println(F("DING!"));


    //Serial.print(F("teste_long:"));  Serial.println(teste_long);


/*
    if (!eeprom->checkHash(100))
    {
        Serial.println(F("Hash FAIL"));
        eeprom->saveHash(100);
        eeprom->save(heya);
        eeprom->save(zomgzor);
        eeprom->save(&teste_long);
        eeprom->save(&teste_int);
        eeprom->save(&teste_byte);
        return;
    }
    char buffer1[30];
    char buffer2[30];
    Serial.println(F("Hash checks out"));
    eeprom->load(buffer1);
    Serial.print(F("buffer:"));  Serial.println(buffer1);
    eeprom->load(buffer2);
    Serial.print(F("buffer:"));  Serial.println(buffer2);
    eeprom->load(&teste_long);
    eeprom->load(&teste_int);
    eeprom->load(&teste_byte);
    
    Serial.print(F("teste_long:"));  Serial.println(teste_long);
    Serial.print(F("teste_int:"));  Serial.println(teste_int);
    Serial.print(F("teste_byte:"));  Serial.println(teste_byte);
    Serial.print(F("teste_float:"));  Serial.println(teste_float);
*/        
    
}

void loop()
{
    
}
