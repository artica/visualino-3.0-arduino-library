/*
Artica CC - http://artica.cc

Byte Variable support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "VariableByte.h"

#include "Utils.h"
#include "DebugConfig.h"


// Default constructor
VariableByte::VariableByte(byte value)
{
	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	spf("new VariableByte:  ");  splvar(value);
	#endif
	this->_value = value;
	this->_initial_value = value;
}


// Create a variable based of a byte array of data. If the data is invalid
// to create the desired variable, returns null
VariableByte* VariableByte::createVariable(byte* data, byte data_length)
{
	// Check if the data buffer has the exact amount of data to create the variable
	if (!testDataLength(data_length, 3)) return NULL;

	// Read the data for this specific variable type
	bool initial_state = data[2];

	VariableByte* variable = new VariableByte( initial_state );	
	// Create the new variable and add it to the devices list
	return variable;
}


// Return the type of data in thes variable
VARIABLE_TYPE VariableByte::getDataType()
{
	return VARIABLE_DATA_BYTE;
}


// Write a new value to the variable
void VariableByte::write(long value)
{
	//spf("VariableByte::write:"); spl(value);
	this->_value = (byte)value;
}


// Read the current value from the variable
long VariableByte::read()
{
	return (long)this->_value;
}


// Set the current value to the default value
void VariableByte::reset()
{
	this->_value = this->_initial_value;
}
