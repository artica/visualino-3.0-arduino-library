/*
Artica CC - http://artica.cc

Analog Output support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/


#ifndef ARTICA_DEVICE_OUTPUT_ANALOG_PIN_H
#define ARTICA_DEVICE_OUTPUT_ANALOG_PIN_H

#include "Arduino.h"
#include "Device.h"
#include "VariableByte.h"

#include "Utils.h"
#include "DebugConfig.h"

// Device for digital pin output
class DeviceOutputAnalogPin:public Device, public VariableByte
{ 
	public:
		// The constructor of the device sets the pin to be used and the initial state
		DeviceOutputAnalogPin( byte pin_number, byte initial_state );
		
		// Initialize the device and obtain the initialization state
		DEVICE_INITIALIZATION initialize();

		// Returns the device type for this class
		DEVICE_TYPE getType() { return VARIABLE_OUTPUT_ANALOG_PIN; };
		
		// Create a device based on a byte array of data. If the data size 
		// is invalid to create the desired device, returns null
		static DeviceOutputAnalogPin* createDevice(byte* data, byte data_length);

		// Prints the device class name
		void printName() { spf("DeviceOutputAnalogPin"); };

		// Update the the hardware pin with the current device value
		void execute();

	// @todo por isto protected depois das mudanças	
	//protected:
		
		// Digital pin used for the device
		byte _pin_number;		
		byte _initial_state;

};

#endif
