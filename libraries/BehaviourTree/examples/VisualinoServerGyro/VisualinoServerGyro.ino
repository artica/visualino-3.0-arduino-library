/*
Artica CC - http://artica.cc

Visualino Server for Gyro - Tarquinio Mota

V0.8 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/


#include "BehaviourTree.h"
#include "EEPROM.h"
#include "EEpromStream.h"
#include "Utils.h"

#ifdef GYRO_ROBOT_SUPPORT
	#include <Gyro.h>
	#include <Motoruino2.h>
	#include <GyroSerialCommand.h>
	#include <Wire.h>
	#include <Metro.h>
	#include <SPI.h>
	#include <Servo.h>
#endif


BehaviourTree* tree;


#define SERIAL_PORT Serial

#define DEBUG_PIN 13


unsigned long last_update = 0;

int command_timeout = 250;

//Metro log_metro = Metro(50);


void setup()
{
    delay(1000);
	// Start the Serial connections at the desired speed
	Serial.begin(115200);
    //while (!Serial) {};

    Serial1.begin(9600);
    //while (!Serial1) {};

    splf("Le starting");
    spf("RAMEND:"); spl(RAMEND);
	
	//delay(1000);
	for (byte i=0; i<2; i++)
	{
		splf("-----------------------------------------------------");
	}

	//ram_tools->printFreeRam();

	// 1828 sem load e run
	// 1811 com load, sem run
	tree = new BehaviourTree();

	// 1805 sem load e run
	// 1788 com load, sem run
	//ram_tools->printFreeRam();

	splf("Tree created...");

	// Set the stream to use to configure the tree
	tree->setStream(&SERIAL_PORT);

	splf("Loading from EEprom...");
	// Load a tree from the EEprom (if it exists)
	tree->loadTreeFromEEprom();
	//ram_tools->printFreeRam();

	splf("Initialization complete!");
}

unsigned long old_time = 0;
unsigned long new_time = 0;
unsigned long last_ping = 0;


void sendPing()
{	
	if (millis()-last_ping < 1000) return;
	last_ping = millis();
	SERIAL_PORT.println("PING");
}


void loop()
{
	// Run the next frame of the tree
	tree->run();

	sendPing();

	//ram_tools->printFreeRam();
	delay(200);
}


