/*
Artica CC - http://artica.cc

Function service support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef ARTICA_SERVICE_FUNCTION_H
#define ARTICA_SERVICE_FUNCTION_H

#include "Arduino.h"
#include "NodeService.h"

// Codes for the different test types
#define FUNCTION_TYPE_COPY			0x00
#define FUNCTION_TYPE_MAP			0x01
#define FUNCTION_TYPE_RANDOM		0x02
#define FUNCTION_TYPE_RANGE			0x03


#define FUNCTION_RANGE_DIRECTION_NULL	0x00
#define FUNCTION_RANGE_DIRECTION_UP		0x01
#define FUNCTION_RANGE_DIRECTION_DOWN	0x02

// Instances to use the delay service
class NodeServiceFunction: public NodeService
{ 
	public:
		// The delay service node contains the node ID and the delay duration
		NodeServiceFunction( NODE_ID id, byte* data, byte data_length, ServiceProvider* service_provider );
		
		// Destructor
		~NodeServiceFunction();

		// Create a node based of a byte array of data. If the data size
		// is invalid to create the desired node, returns null
		static NodeServiceFunction* createNode(byte* data, byte data_length, ServiceProvider* service_provider);

		// Always returns success (the functions are evaluated "instantly")
		NODE_STATE run();

		// Initialize any functions that need to maintain state
		//void start();


		// Prints the node class name
		void printName() { spf("NodeServiceFunction"); };

		// Variables specifically for the Range function
		byte _range_direction;
		unsigned long _range_value;

};

#endif
