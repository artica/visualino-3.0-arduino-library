#ifndef EEPROM_STREAM_H
#define EEPROM_STREAM_H
#include <../EEPROM/EEPROM.h>

#include "Arduino.h"
class EEpromStream: public Stream
{
public:
	// Creates a new EEpromStream object, with a specific max size
	EEpromStream(unsigned int size);

	// Get the size of the EEprom
	unsigned int getSize();
	// Sets the read pointer to a specific position on the EEprom
	void setReadPointer(unsigned int pointer);
	// Sets the write pointer to a specific position on the EEprom
	void setWritePointer(unsigned int pointer);

	// Get the current position of the read pointer
	unsigned int getReadPointer();
	// Get the current position of the write pointer
	unsigned int getWritePointer();

	// ------------------------------------------------------------------
	// Methods to implement from the Stream class
	// ------------------------------------------------------------------

	// Returns the number of bytes from the current read position untill 
	// the final EEprom position allocated to the stream
    int available();
    // Reads the next byte from the eeprom (or -1 if there are no more bytes to read)
    int read();
    // Reads the next value of the stream, without moving to the next one
    int peek();
    // This methos sets the read pointer to the end of the memory. read() will always
    // return -1 untill we call setReadPointer with a valid memory position
    void flush();

	// ------------------------------------------------------------------
	// Methods to implement from the Print class
	// ------------------------------------------------------------------
	// Writes a byte to the next position on the eeprom
    size_t write(uint8_t);
  	
	// ------------------------------------------------------------------
	// Methods to read and write specific types of data (in binary mode)
	// because the print class only supports writting byte values
	// ------------------------------------------------------------------
	void read(char *text);
	void write(char *text);
	void read(byte *value);
	void write(byte *value);
	void read(int *value);
	void write(int *value);
	void read(unsigned int *value);
	void write(unsigned int *value);
	void read(long *value);
	void write(long *value);
	void read(unsigned long *value);
	void write(unsigned long *value);
	void read(float *value);
	void write(float *value);


private:
	// Updates the value of a specific position on the eeprom 
	// (only writes if the data currently there is different form the new one)
	void eepromUpdate( byte *value );
	// Size of the EEprom to use (starting at position 0)
	unsigned int _size;
	// Position to read the next byte from the eeprom
	unsigned int _read_pointer;
	// Position to write the next byte on the eeprom
	unsigned int _write_pointer;
};

#endif
