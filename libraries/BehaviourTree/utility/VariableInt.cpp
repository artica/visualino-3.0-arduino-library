/*
Artica CC - http://artica.cc

Int Variable support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/


#include "VariableInt.h"

#include "Utils.h"
#include "DebugConfig.h"


// Default constructor
VariableInt::VariableInt(int value)
{
	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	spf("new VariableInt:  ");  splvar(value);
	#endif
	this->_value = value;
	this->_initial_value = value;
}


// Create a variable based of a byte array of data. If the data is invalid
// to create the desired variable, returns null
VariableInt* VariableInt::createVariable(byte* data, byte data_length)
{
	// Check if the data buffer has the exact amount of data to create the variable
	if (!testDataLength(data_length, 4)) return NULL;

	// Read the data for this specific variable type
	bool initial_state = data[2];

	VariableInt* variable = new VariableInt( initial_state );
	// Create the new variable and add it to the devices list
	return variable;	
}


// Return the type of data in thes variable
VARIABLE_TYPE VariableInt::getDataType()
{
	return VARIABLE_DATA_INT;
}


// Write a new value to the variable
void VariableInt::write(long value)
{
	//spf("VariableInt::write:"); spl(value);
	this->_value = (int)value;
}


// Read the current value from the variable
long VariableInt::read()
{
	return (long)this->_value;
}


// Set the current value to the default value
void VariableInt::reset()
{
	this->_value = this->_initial_value;
	//spf("VariableInt::reset:"); spl(this->_initial_value);
}
