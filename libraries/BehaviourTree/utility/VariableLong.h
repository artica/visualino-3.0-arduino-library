/*
Artica CC - http://artica.cc

Long Variable support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/


#ifndef ARTICA_VARIABLE_LONG_H
#define ARTICA_VARIABLE_LONG_H

#include "Arduino.h"
#include "Variable.h"

#include "Utils.h"
#include "DebugConfig.h"


// Generic class to represent a variable
class VariableLong: public Variable
{
	public:
		// Default constructor
		VariableLong() {};
				 
		// Constructor with initialization
		VariableLong(long value);		

		// Returns the device type for each different service
		VARIABLE_TYPE getDataType();
		
		// Prints the device class name
		void printName() { spf("VariableLong"); };

		// Create a variable based on a byte array of data. If the data size 
		// is invalid to create the desired variable, returns null
		static VariableLong* createVariable(byte* data, byte data_length);

		// Write a new value to the variable
		void write(long value);

		// Read the current value from the variable
		long read();

		// Set the current value to the default value
		void reset();	


	protected:
		long _value;
		long _initial_value;
};

#endif
