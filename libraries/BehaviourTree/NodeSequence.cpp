/*
Artica CC - http://artica.cc

Sequence node - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "NodeSequence.h"

#include "Utils.h"


#include "DebugConfig.h"

// We need to specify an id and the maximum number of children
// when initializing a sequence node
NodeSequence::NodeSequence(byte id, byte max_children, unsigned long total_iterations):
	NodeInternal(id, max_children)
{
	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	printName(); spf("  max children:"); spl(max_children); delay(10);
	#endif
	this->_type = NODE_SEQUENCE;	
	this->_total_iterations = total_iterations;
}


// Destructor
NodeSequence::~NodeSequence()
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
	splf("~NodeSequence"); delay(DEBUG_DELAY);
	#endif
}


// Create a node based of a byte array of data. If the data size
// is invalid to create the desired node, returns null
NodeSequence* NodeSequence::createNode(byte* data, byte data_length)
{
	spf("NodeSequence::createNode data_length:"); spl(data_length);
	// Check if the data buffer has the exact amount of data to create the node
	if (!testDataLength(data_length, 7)) return NULL;

	// The first byte contains the node id
	NODE_ID node_id = data[0];
	// The third byte contains the number of children
	byte child_count = data[2];

	// On the third byte starts a 32-bit value with the desired number of repetitions
	unsigned long total_iterations = getUInt32FromArray(data, 3);

	spf("total_iterations:"); spl(total_iterations);
	
	NodeSequence* node = new NodeSequence( node_id, child_count, total_iterations );
	// Create the new node and add it to the node list
	return node;

	//return NULL;
}


// Go through the children nodes, running them in sequence
NODE_STATE NodeSequence::run()
{
	Node::run();
	// Let's go through all the nodes, until one of them returns failure or running
	while (true)
	{
		// Did all the childern nodes succeded?
		if (this->_current_child >= this->_max_children) 
		{
			// We have completed another iteration...
			this->_current_iteration++;

			// Now let's see if we need to start another iteration
			// _total_iterations == 0 means we want to loop forever
			if (this->_total_iterations == 0 || this->_current_iteration < this->_total_iterations)
			{
				// Restart running the node, marking all the child nodes as inactive
				this->_current_child = 0;
				this->stop(NODE_STATE_INACTIVE);
				return NODE_STATE_RUNNING;
			}
			// If we reached the final iteration, return a success
			else
			{
				return this->stop(NODE_STATE_SUCCESS);
			}
		}
		//spf("running child:"); spl(this->_current_child); delay(DEBUG_DELAY);
		// Run the next child node and get the running state
		//if (this->_id == 2) { splf("running node 2"); delay(DEBUG_DELAY); }

		NODE_STATE state = this->_children[this->_current_child]->run();

		//spf("BAM!:"); spl(this->_current_child); delay(DEBUG_DELAY);
		#ifdef BEHAVIOUR_TREE_DEBUG_RUN
		spf("child:"); sp(this->_current_child); spf("  state:"); spl(state); delay(DEBUG_DELAY);
		#endif
			
		switch (state)
		{
			// The child node is still running, so we return NODE_STATE_RUNNING
			// to the parent node and don't change anything else
			case NODE_STATE_RUNNING:
				return NODE_STATE_RUNNING;
			break;
			// The child node finished running successfully, so we advance to 
			// the next child. If this is the last one, return NODE_STATE_SUCCESS
			case NODE_STATE_SUCCESS:
				this->_current_child++;
				//else return NODE_STATE_RUNNING;
			break;
			// The child node failed, so the sequence node also fails
			case NODE_STATE_FAILURE:
				this->stop(NODE_STATE_FAILURE);
				return NODE_STATE_FAILURE;
			break;
		}	
	}	
}



// Sets the next child to run as the first one
void NodeSequence::start()
{
	NodeInternal::start();

	this->_current_iteration = 0;
}


