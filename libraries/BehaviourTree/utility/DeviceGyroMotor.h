/*
Artica CC - http://artica.cc

Gyro Motor support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef ARTICA_DEVICE_GYRO_MOTOR_H
#define ARTICA_DEVICE_GYRO_MOTOR_H

#include "Arduino.h"
#include "DeviceGyro.h"
#include "VariableInt.h"

#include "Utils.h"
#include "DebugConfig.h"

#define GYRO_MOTOR_POSITION_LEFT 1
#define GYRO_MOTOR_POSITION_RIGHT 2

// Device for a differential motor output
class DeviceGyroMotor:public DeviceGyro, public VariableInt
{
	public:
		// The constructor of the device sets the pins to be used for both motors
		DeviceGyroMotor( byte motor_position, int initial_value, ServiceProvider* service_provider);
		
		// Initialize the device and obtain the initialization state
		DEVICE_INITIALIZATION initialize();

		// Returns the device type for this class
		DEVICE_TYPE getType() {	return VARIABLE_GYRO_OUTPUT_MOTOR; };
		
		// Create a device based on a byte array of data. If the data size 
		// is invalid to create the desired device, returns null
		static DeviceGyroMotor* createDevice(byte* data, byte data_length, ServiceProvider* service_provider );

		// Prints the device class name
		void printName() { spf("DeviceGyroMotor"); };
	
		// Update the the hardware pin with the current device value
		void execute();

	// @todo por isto protected depois das mudanças	
	//protected:

		// Position of the motor (LEFT or RIGHT)
		byte _motor_position;

};

#endif
