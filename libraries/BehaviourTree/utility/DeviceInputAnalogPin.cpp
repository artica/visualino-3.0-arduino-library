/*
Artica CC - http://artica.cc

Analog Input support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "DeviceInputAnalogPin.h"

#include "Utils.h"
#include "DebugConfig.h"


// Constructor of the device sets the pin to be used 
DeviceInputAnalogPin::DeviceInputAnalogPin( byte pin_number ):
	VariableInt(0)

{
	this->_pin_number = pin_number;
	this->initialize();
}


// Initialize the device and obtain the initialization state
DEVICE_INITIALIZATION DeviceInputAnalogPin::initialize()
{
	// Initializing this device means simply configuring the pin as input

	// @todo acertar or pinos aqui...
	pinMode(this->_pin_number, INPUT);

	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	printName(); spf("  pin:"); spl(this->_pin_number);
	#endif
	return DEVICE_INITIALIZATION_SUCCESSFUL;
}


// Create a device based on a byte array of data. If the data is invalid
// to create the desired device, returns null
DeviceInputAnalogPin* DeviceInputAnalogPin::createDevice(byte* data, byte data_length)
{
	// Check if the data buffer has the exact amount of data to create the device
	if (!testDataLength(data_length, 3)) return NULL;

	// Read the pin number to use for this device
	byte pin = data[2];

	DeviceInputAnalogPin* device = new DeviceInputAnalogPin( pin );
	// Create the new device and add it to the devices list
	return device;
}


// Update the value with new data from the sensor
void DeviceInputAnalogPin::update()
{
	// Get the raw state from the pin
	this->_value = analogRead(this->_pin_number);

	//spf("Update:"); spl(this->_value);
}
