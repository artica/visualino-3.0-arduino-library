/*
Artica CC - http://artica.cc

Behaviour Tree Core - Tarquinio Mota

V0.8 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef ARTICA_BEHAVIOUR_TREE_H
#define ARTICA_BEHAVIOUR_TREE_H


// Time between two main loops in millis (this includes the time the loop takes to run)
// This is important to limit the maximum number of messages sent to the browser
// when the program running on the tree constantly changes state
#define LOOP_DELAY 20


#include "Arduino.h"
#include "Utils.h"
#include "ServiceProvider.h"

#include "Node.h"

#include "Utils.h"
#include "DebugConfig.h"


// Commands used by the interface to interact with the tree
#define COMMAND_LOAD_TREE 'L'
#define COMMAND_SAVE_TREE 'E'
#define COMMAND_DELETE_TREE 'D'
#define COMMAND_RUN_TREE 'R'
#define COMMAND_STOP_TREE 'S'
#define COMMAND_PAUSE_TREE 'P'
#define COMMAND_STEP_TREE 'F'


// @todo these should be obsolete when the new communication library is used
// Separator used to divide the message data from the hash
#define COMMAND_START_HASH '|'
// Separator used to divide different commands
#define COMMAND_SEPARATOR ';'


// Possible encoding types for the data stream
// Data is received from the computer in hex, to be human readable-ish,
// but stored in binary on the EEprom, to save space
enum TREE_ENCODING
{
	TREE_ENCODING_BINARY = 0,
	TREE_ENCODING_HEX= 1
};


typedef unsigned int TREE_ID;

// Main BehaviourTree class object
class BehaviourTree
{ 
	public:
		// The default constructor creates a new empty tree
		BehaviourTree();
		// Destructor
		~BehaviourTree();

		// Initialize all the variables before running a new tree
		void init(byte max_variables, byte max_nodes, TREE_ID tree_id);
		// Reset the tree, freeing all allocated memory from the current tree
		void reset();

		// Initializes the tree, based on a byte buffer containing the tree
		// maximum number of devices and nodes
		// Returns false if the data buffer is invalid
		bool initTree( byte* data, byte data_length);

		// Creates a new tree, based on a byte buffer containing the tree
		// maximum number of devices and nodes
		// Returns null if the data buffer is invalid
		bool createTree( byte* data, byte data_length);

		// Adds a new node to the behaviour tree. "data" contains all 
		// the node configuration information encoded in a byte array
		// The first node added is assumed to be the root node
		bool addNode(byte* data, byte data_length);

		// Adds a new node to the tree node list
		// Returns false if the node or node id are invalid
		bool addNode(NODE_ID id, Node* node);

		// Adds a new service instance node to the tree node list
		// (this is a leaf node that maintains an instance of a service)
		// "data" contains only the data relative to the service
		// Returns false if the node or node id are invalid
		Node* createNodeService( byte* data, byte data_length);
	
		// Adds a new connection between two nodes. "data" contains the
		// ids of both nodes, the parent is always the first
		bool addConnection(byte* data, byte data_length);

		// Adds a new element (device/node/connection) to a tree
		// "data" contains information on what the new element is, 
		// and the necessary data to initialize it
		// Returns false if the element or elementa data are invalid
		bool processData(byte* buffer, byte buffer_length);

		// Loads and returns a tree from an incoming data stream
		// encoding_type specifies if the data is in hexadecimal or binary format
		// save specifies if the tree should be saved to the EEprom
		// Returns true if the tree has loaded succesefully,
		bool loadTreeFromStream(Stream* stream, TREE_ENCODING encoding_type, bool save);

		// debug 
		bool loadTreeFromStream2(Stream* stream, TREE_ENCODING encoding_type, bool save);

		// Loads a new tree from the internal EEprom memory
		// Returns true if a valid tree was loaded
		bool loadTreeFromEEprom();

		// Deletes any tree from the internal EEprom memory
		void deleteTreeFromEEprom();

		// Loads and returns a tree from the EEprom
		// Returns a null pointer in case of error
		//static BehaviourTree*  loadTreeFromEEprom();

		// Run the BehaviourTree
		// This should be called often of the loop() to ensure the tree runs propperly
		NODE_STATE run();

		// Methods to control the running state of the tree
		// Starts executing the tree. Has no effect if the tree is already running
		void start();
		// Pauses the execution of the tree. After a pause(), start() and step()
		// will continue the execution from the same point where the pause happend
		void pause();
		// Stops the execution of the tree. After a stop, calling start() or step()
		// will start executing the tree from the beginning
		void stop();
		// Runs a single step of the tree, continuing from the last position	
		// If the tree is running, it will be paused after this	
		void step();


		// Set the stream which is going to control the tree
		void setStream(Stream* stream);

		// Run the next frame of the tree
		NODE_STATE runNextFrame();

		// Reports the status of the nodes to the data stream
		void report();

		// Parse the commands from the incoming data strean
		void parseCommand();

		// Calculate a hash value (unsigned int) for a string
		static unsigned int calculateHash(char* str);



	private:

		// Flushes all the data from the incoming data stream
		void flushStream();

		// Data stream to control the tree
		Stream* _stream;

		// Service provider used to supply services to the nodes
		ServiceProvider* _service_provider;

		// Returns a pointer to the node with a specific ID, or null if
		// the id does not match any node.
		Node* getNode(NODE_ID id);
	
		// Link for the root node of the tree
		Node* _root;

		// Node list for the tree (instanced only when loading the tree)
		Node** _nodes;
		// Maximum number of nodes on the node list
		byte _max_nodes;

		// Hash value that it associated with the current tree
		
		TREE_ID _tree_id;

		// A tree is complete when it has finished loading
		// We can only run a tree after it's complete
		bool _complete;
		// Did the tree change in the last frame?
		bool _changed;

		// Is the tree currently active
		bool _active;
		// Is the tree currently paused
		bool _paused;
		// Is the tree supposed to advance a single step
		bool _stepping;

		// Time (in microseconds) the last node has run
		unsigned long _time_last_loop;
		
		// Counter for the number of re-transmission requests
		byte _transmit_retry_count;
};

#endif
