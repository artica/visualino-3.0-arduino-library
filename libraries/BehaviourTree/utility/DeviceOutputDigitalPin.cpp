/*
Artica CC - http://artica.cc

Digital Output support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/


#include "DeviceOutputDigitalPin.h"

#include "Utils.h"
#include "DebugConfig.h"


// Constructor of the device sets the pin to be used 
DeviceOutputDigitalPin::DeviceOutputDigitalPin( byte pin_number, bool initial_state ):
	VariableBool(initial_state)
{
	this->_pin_number = pin_number;
	this->initialize();
}


// Initialize the device and obtain the initialization state
DEVICE_INITIALIZATION DeviceOutputDigitalPin::initialize()
{
	// Initializing this device means simply configuring the pin as output
	pinMode(this->_pin_number, OUTPUT);
	digitalWrite(this->_pin_number, this->_initial_state);
	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	printName(); spf("  pin:"); sp(this->_pin_number);  spf("  init:"); spl(this->_initial_state);
	#endif
	return DEVICE_INITIALIZATION_SUCCESSFUL;
}


// Create a device based of a byte array of data. If the data is invalid
// to create the desired device, returns null
DeviceOutputDigitalPin* DeviceOutputDigitalPin::
	createDevice(byte* data, byte data_length)
{
	// Check if the data buffer has the exact amount of data to create the device
	if (!testDataLength(data_length, 4)) return NULL;

	// Read the data for this specific device type
	byte pin = data[2];
	bool initial_state = data[3];

	DeviceOutputDigitalPin* device = new DeviceOutputDigitalPin( pin, initial_state );

	// Create the new device and add it to the devices list
	return device;	
}


// Update the the hardware pin with the current device value
void DeviceOutputDigitalPin::execute()
{
	// Simply write the correct value to the device
	digitalWrite(this->_pin_number, this->_value);
}
