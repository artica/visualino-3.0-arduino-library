/*
Artica CC - http://artica.cc

Generic variable type - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/


#ifndef ARTICA_VARIABLE_H
#define ARTICA_VARIABLE_H

#include "Arduino.h"
#include "MessageConstants.h"

#include "Utils.h"
#include "DebugConfig.h"


// Set the data type used for variable types
typedef byte VARIABLE_TYPE;

// Set the data type used for device ids
typedef byte VARIABLE_ID;


// Generic class to represent a variable
class Variable
{
	public:
		// Default constructor
		Variable();
				 
		// Default destructor
		virtual ~Variable();

		// Returns the device type for each different service
		virtual VARIABLE_TYPE getDataType(){};
		
		// Prints the device class name
		void printName() { spf("Variable"); };


		// Update the value with new data from the hardware (when appliable)
		virtual void update() {};

		// Update the hardware state based on the current device value (when appliable)
		virtual void execute() {};

		// Write a new value to the variable
		virtual void write(long value) {};

		// Read the current value from the variable
		virtual long read() {return 0;};

		// Set the value to the default value
		virtual void reset() {};

		// Checks is the variable value has changed since the last check
		virtual bool changed() { return true;};


	protected:	
		// Tests if a data length value is the exact number of bytes
		// necessary to create a device of a specific type
		static bool testDataLength(byte data_length, byte data_necessary);

};

#endif
