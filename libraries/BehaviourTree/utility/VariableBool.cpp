/*
Artica CC - http://artica.cc

Boolean Variable support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/


#include "VariableBool.h"

#include "Utils.h"
#include "DebugConfig.h"


// Default constructor
VariableBool::VariableBool( bool value )
{
	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	spf("new VariableBool:  "); splvar(value);
	#endif
	this->_value = value;
	this->_initial_value = value;
}


// Create a variable based of a byte array of data. If the data is invalid
// to create the desired variable, returns null
VariableBool* VariableBool::createVariable(byte* data, byte data_length)
{
	// Check if the data buffer has the exact amount of data to create the variable
	if (!testDataLength(data_length, 3)) return NULL;

	// Read the data for this specific variable type
	bool initial_state = data[2];

	VariableBool* variable = new VariableBool( initial_state );
	// Create the new variable and add it to the devices list
	return variable;
}


// Write a new value to the variable
void VariableBool::write(long value)
{
	//spf("VariableBool::write:"); spl(value);
	this->_value = (bool)value;
}


// Read the current value from the variable
long VariableBool::read()
{
	return (long)this->_value;
}


// Set the current value to the default value
void VariableBool::reset()
{
	this->_value = this->_initial_value;
}
