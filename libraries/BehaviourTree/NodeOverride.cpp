/*
Artica CC - http://artica.cc

Override node - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "NodeOverride.h"

#include "Utils.h"
#include "DebugConfig.h"

// This node type only needs the id to be initialized
NodeOverride::NodeOverride(byte id, byte policies): 
	NodeInternal(id, 1)
{

	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	printName(); spf(" policies: ");
	// Print the policies in use for the overrides
	spf("  F->"); Node::printStateName( (NODE_STATE) ((this->_policies & NODE_OVERRIDE_POLICY_FAILURE_MASK) >> NODE_OVERRIDE_POLICY_FAILURE_OFFSET) );
	spf("  R->"); Node::printStateName( (NODE_STATE) ((this->_policies & NODE_OVERRIDE_POLICY_RUNNING_MASK) >> NODE_OVERRIDE_POLICY_RUNNING_OFFSET) );
	spf("  S->"); Node::printStateName( (NODE_STATE) ((this->_policies & NODE_OVERRIDE_POLICY_SUCCESS_MASK) >> NODE_OVERRIDE_POLICY_SUCCESS_OFFSET) );
	spl();
	#endif
	this->_policies = policies;
	this->_type = NODE_OVERRIDE;
}



// Create a node based of a byte array of data. If the data size
// is invalid to create the desired node, returns null
NodeOverride* NodeOverride::createNode(byte* data, byte data_length)
{
	// Check if the data buffer has the exact amount of data to create the node
	if (!testDataLength(data_length, 3)) return NULL;

	// The first byte contains the node id
	NODE_ID node_id = data[0];
	// The third byte contains the policies configuration
	byte policies = data[2];
	
	NodeOverride* node = new NodeOverride( node_id, policies );
	// Create the new node and add it to the node list
	return node;
}

// Run the child node and override the result if necessary
NODE_STATE NodeOverride::run()
{
	Node::run();
	// Run the child node and get the running state
	NODE_STATE state = this->_children[0]->run();

	#ifdef BEHAVIOUR_TREE_DEBUG_RUN
	printName(); spf(" child state:"); spl(state);
	#endif		

	// Get the override policy for the current node state
	byte override_state = this->_policies;

	// Get the desired override state for the child node returned state
	switch (state)
	{
		case NODE_STATE_FAILURE:
			override_state =  (override_state & NODE_OVERRIDE_POLICY_FAILURE_MASK)			
								>> NODE_OVERRIDE_POLICY_FAILURE_OFFSET;
		break;
		case NODE_STATE_RUNNING:
			override_state = ( override_state & NODE_OVERRIDE_POLICY_RUNNING_MASK) 
										>> NODE_OVERRIDE_POLICY_RUNNING_OFFSET;
		break;
		case NODE_STATE_SUCCESS:
			override_state = (override_state & NODE_OVERRIDE_POLICY_SUCCESS_MASK) 
										>> NODE_OVERRIDE_POLICY_SUCCESS_OFFSET;										
	}

	// Let's see if the node state needs override
	if (state != override_state)
	{
		#ifdef BEHAVIOUR_TREE_DEBUG_RUN
		printName(); sp(':'); sp(this->_id); spf("  state:"); sp(state); spf(" -> "); spl(override_state);
		#endif		
	}
	// If the node stopped running, we need to call the stop method
	if (override_state != NODE_STATE_RUNNING) this->stop((NODE_STATE)override_state);
	// Still running, just return the state (that should be NODE_STATE_RUNNING by now)
	return (NODE_STATE)override_state;	
}


