/*
Artica CC - http://artica.cc

Delay service support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef ARTICA_SERVICE_DELAY_H
#define ARTICA_SERVICE_DELAY_H

#include "Arduino.h"
#include "NodeService.h"

// Instances to use the delay service
class NodeServiceDelay: public NodeService
{ 
	public:
		// The delay service node contains the node ID and the delay duration
		NodeServiceDelay( NODE_ID id, byte* data, byte data_length, ServiceProvider* service_provider );
		
		// Destructor
		~NodeServiceDelay();

		// Create a node based of a byte array of data. If the data size
		// is invalid to create the desired node, returns null
		static NodeServiceDelay* createNode(byte* data, byte data_length, ServiceProvider* service_provider);

		// Returns NODE_STATE_SUCCESS if enough time has passed since the 
		// delay started, NODE_STATE_RUNNING otehrwise
		NODE_STATE run();

		// Set the start time os the delay as the current time
		void start();

		// Prints the node class name
		void printName() { spf("NodeServiceDelay"); };
			
	private:
		// Time when the delay started
		unsigned long _start_time;
};

#endif
