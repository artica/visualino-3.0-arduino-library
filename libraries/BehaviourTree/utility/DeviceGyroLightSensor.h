/*
Artica CC - http://artica.cc

Gyro Distance Sensor support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef ARTICA_DEVICE_GYRO_LIGHT_SENSOR_H
#define ARTICA_DEVICE_GYRO_LIGHT_SENSOR_H

#include "Arduino.h"
#include "DeviceGyro.h"
#include "VariableLong.h"

#include "Utils.h"
#include "DebugConfig.h"

// Possible positions for the light sensors
#define GYRO_LIGHT_SENSOR_POSITION_LEFT 1
#define GYRO_LIGHT_SENSOR_POSITION_RIGHT 2
#define GYRO_LIGHT_SENSOR_POSITION_CENTER 3

// Working mode for the light sensor
// In analog mode it outputs the raw data read from the sensor
#define GYRO_LIGHT_SENSOR_DATA_MODE_ANALOG 1
// In digital mode it outputs zero or one, by comparing the 
// value read with a configurable threshold
#define GYRO_LIGHT_SENSOR_DATA_MODE_DIGITAL 2


// Device for a light sensor input
class DeviceGyroLightSensor:public DeviceGyro, public VariableLong
{
	public:
		// The constructor of the device sets the pins to be used for both motors
		DeviceGyroLightSensor( byte position, byte data_type, int threshold, ServiceProvider* service_provider);
		
		// Initialize the device and obtain the initialization state
		DEVICE_INITIALIZATION initialize();

		// Returns the device type for this class
		DEVICE_TYPE getType() {	return VARIABLE_GYRO_INPUT_DISTANCE_SENSOR; };
		
		// Create a device based on a byte array of data. If the data size 
		// is invalid to create the desired device, returns null
		static DeviceGyroLightSensor* createDevice(byte* data, byte data_length, ServiceProvider* service_provider );

		// Prints the device class name
		void printName() { spf("DeviceGyroLightSensor"); };
	
		// Update the the value of the device with a value read from the sensor
		void update();

	// @todo por isto protected depois das mudanças	
	//protected:

		// Position of the SENSOR (LEFT, RIGHT or BELOW)
		byte _position;
		// Data mode to communicate the values (ANALOG or DIGITAL)
		byte _data_type;
		// Threshold value to assume zero or one (only in DIGITAL mode)
		int _threshold;

};

#endif
