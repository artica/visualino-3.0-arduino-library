/*
Artica CC - http://artica.cc

Test service support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "ServiceTest.h"

#include "Utils.h"

// Position of the different data variables in the generic service data aray
#define SERVICE_TEST_BUFFER_OVERRIDE_POSITION 0
#define SERVICE_TEST_BUFFER_TEST_POSITION 1

// A service instance node contains a node id and a pointer to the service provider
// The data buffer contains different data depending on the node type
NodeServiceTest::NodeServiceTest( NODE_ID id, byte* data, byte data_length, ServiceProvider* service_provider ):
	NodeService(id, data, data_length, service_provider)
{
	// Get the type of test to initialize
	byte test = this->_data[SERVICE_TEST_BUFFER_TEST_POSITION];

	// Not all test need to have persistent data. We only allocate the buffer
	// depending on the node types
	switch (test)
	{
		case TEST_TYPE_INCREASE:
		case TEST_TYPE_DECREASE:
		case TEST_TYPE_CHANGE:
		{
			splf("Initializing TEST_TYPE_INCREASE");
			//We need to allocate a long to keep the old value between readings
			this->_persistent_data = (byte*) malloc(sizeof(long));
		}
	}
}


// Destructor
NodeServiceTest::~NodeServiceTest()
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
	splf("~NodeServiceTest");
	#endif

	// Get the type of test to initialize
	byte test = this->_data[SERVICE_TEST_BUFFER_TEST_POSITION];

	// Not all test need to have persistent data. We only free the buffer
	// depending on the node types
	switch (test)
	{
		case TEST_TYPE_INCREASE:
		case TEST_TYPE_DECREASE:
		case TEST_TYPE_CHANGE:
		{
			free(this->_persistent_data);
		}
	}
} 


// Create a node based of a byte array of data. If the data size
// is invalid to create the desired node, returns null
NodeServiceTest* NodeServiceTest:: createNode(byte* data, byte data_length, ServiceProvider* service_provider)
{
	// Test nodes have different data lenghts depending on the test type
	// and the data types involved in the test
	byte node_data_length = data[3];

	// Check if the data buffer has the exact amount of data to create the node
	if (!testDataLength(data_length, node_data_length+4)) return NULL;

	// The first byte of the array contains the node id
	NODE_ID node_id = data[SERVICE_TEST_BUFFER_OVERRIDE_POSITION];
			
	NodeServiceTest* node = new NodeServiceTest(node_id, &(data[4]), node_data_length, service_provider);	
	// At this point everything should be fine, so let's create the node
	return node;
}


// Returns NODE_STATE_SUCCESS if enough time has passed since the 
// delay started, NODE_STATE_RUNNING otherwise
NODE_STATE NodeServiceTest::run()
{
	Node::run();

	// The first variable data should start immediatlly after the test type byte
	byte pos = SERVICE_TEST_BUFFER_TEST_POSITION+1;

	long value_1 = this->_service_provider->getValueFromBuffer(this->_data, &pos);
	long value_2 = this->_service_provider->getValueFromBuffer(this->_data, &pos);

	//spvar(value_1); splvar(value_2);

	NODE_STATE state = NODE_STATE_RUNNING;

	// Get the type of test to perform
	byte test = this->_data[SERVICE_TEST_BUFFER_TEST_POSITION];
	switch (test)
	{
		// Test if two values are equal
		case TEST_TYPE_EQUAL:
		{
			if (value_1 == value_2) state = NODE_STATE_SUCCESS;
			else state = NODE_STATE_FAILURE;
		}
		break;

		// Test if two values are different
		case TEST_TYPE_DIFFERENT:
		{
			if (value_1 != value_2) state = NODE_STATE_SUCCESS;
			else state = NODE_STATE_FAILURE;
		}
		break;

		// Test if a value is bigger than the other
		case TEST_TYPE_BIGGER:
		{
			if (value_1 > value_2) state = NODE_STATE_SUCCESS;
			else state = NODE_STATE_FAILURE;
		}
		break;

		// Test if a value is smaller than the other
		case TEST_TYPE_SMALLER:
		{
			if (value_1 < value_2) state = NODE_STATE_SUCCESS;
			else state = NODE_STATE_FAILURE;
		}
		break;
		
		// Test if a value is inside a range (defined by two other values)
		case TEST_TYPE_INSIDE:
		{
			long value_3 = this->_service_provider->getValueFromBuffer(this->_data, &pos);
			if (value_1 >= value_2 && value_1 <= value_3) state = NODE_STATE_SUCCESS;
			else state = NODE_STATE_FAILURE;
		}
		break;

		// Test if a value is outside a range (defined by two other values)
		case TEST_TYPE_OUTSIDE:
		{
			long value_3 = this->_service_provider->getValueFromBuffer(this->_data, &pos);
			if (value_1 < value_2 || value_1 > value_3) state = NODE_STATE_SUCCESS;
			else state = NODE_STATE_FAILURE;
		}
		break;

		// Test if a value has increased more than a threshhold since the last test
		case TEST_TYPE_INCREASE:
		{
			// Read the value from the last iteration of the test
			long initial_value = getUInt32FromArray(this->_persistent_data, 0);

			// Check if the value increased more than the desired threshold
			if (value_1 >= initial_value+value_2) state = NODE_STATE_SUCCESS;
			else state = NODE_STATE_FAILURE;

			// Store the value read on this iteration
			putInt32InArray(value_1, this->_persistent_data, 0);
		}
		break;

		// Test if a value has decreased more than a threshhold since the last test
		case TEST_TYPE_DECREASE:
		{
			// Read the value from the last iteration of the test
			long initial_value = getUInt32FromArray(this->_persistent_data, 0);

			// Check if the value decreased more than the desired threshold
			if (value_1 <= initial_value-value_2) state = NODE_STATE_SUCCESS;
			else state = NODE_STATE_FAILURE;

			// Store the value read on this iteration
			putInt32InArray(value_1, this->_persistent_data, 0);
		}
		break;

		// Test if a value has changed more than a threshhold since the last test
		case TEST_TYPE_CHANGE:
		{
			// Read the value from the last iteration of the test
			long initial_value = getUInt32FromArray(this->_persistent_data, 0);

			// Check if the value decreased more than the desired threshold
			if (value_1 <= initial_value-value_2 || value_1 >= initial_value+value_2) state = NODE_STATE_SUCCESS;
			else state = NODE_STATE_FAILURE;

			// Store the value read on this iteration
			putInt32InArray(value_1, this->_persistent_data, 0);
		}
		break;

	}


	//byte override_state = state;


	// Get the override policy for the node
	byte override_state = this->_data[SERVICE_TEST_BUFFER_OVERRIDE_POSITION];
	
	//spf("override_state:"); spbl(override_state);

	// Get the desired override state for the child node returned state
	switch (state)
	{
		case NODE_STATE_FAILURE:
			override_state =  (override_state & NODE_OVERRIDE_POLICY_FAILURE_MASK)			
								>> NODE_OVERRIDE_POLICY_FAILURE_OFFSET;
		break;
		
		case NODE_STATE_RUNNING:
			override_state = ( override_state & NODE_OVERRIDE_POLICY_RUNNING_MASK) 
										>> NODE_OVERRIDE_POLICY_RUNNING_OFFSET;
		break;
		
		case NODE_STATE_SUCCESS:
			override_state = (override_state & NODE_OVERRIDE_POLICY_SUCCESS_MASK)
										>> NODE_OVERRIDE_POLICY_SUCCESS_OFFSET;
		break;
	}


	#ifdef BEHAVIOUR_TREE_DEBUG_RUN
	// Let's see if the node state needs override
	if (state != override_state)
	{
		printName(); sp(':'); sp(this->_id); spf("  state:"); sp(state); spf(" -> "); spl(override_state);
	}
	#endif

	// If the node stopped running, we need to call the stop method
	if (override_state != NODE_STATE_RUNNING) this->stop((NODE_STATE)override_state);
	// Still running, just return the state (that should be NODE_STATE_RUNNING by now)
	return (NODE_STATE)override_state;	
}

// Starts the node execution
void NodeServiceTest::start()
{
	Node::start();

	//spf("********************************Starting a NodeServiceTest:"); spl(this->_id);

	// Get the type of test to initialize
	byte test = this->_data[SERVICE_TEST_BUFFER_TEST_POSITION];

	// Not all test need to have persistent data. 
	switch (test)
	{
		// The INCREASE/DECREASE/CHANGE nodes compare the current value from
		// the elected data source with the initial value. So we need to get
		// and store that initial base value
		case TEST_TYPE_INCREASE:
		case TEST_TYPE_DECREASE:
		case TEST_TYPE_CHANGE:
		{
			// The first variable data should start immediatlly after the test type byte
			byte pos = SERVICE_TEST_BUFFER_TEST_POSITION+1;
			long value = this->_service_provider->getValueFromBuffer(this->_data, &pos);
			putInt32InArray(value, this->_persistent_data, 0);
			//spf("Initial value:"); spl(value);
		}
	}

}


