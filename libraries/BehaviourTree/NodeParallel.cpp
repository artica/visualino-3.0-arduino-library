/*
Artica CC - http://artica.cc

Parallel node - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "NodeParallel.h"

#include "Utils.h"
#include "DebugConfig.h"

// Bit masks for the completion and failure policies
// (the data is all stores in the _policies variable)
#define NODE_PARALELL_POLICY_COMPLETION 0x01
#define NODE_PARALELL_POLICY_FAILURE 0x02

// We need to specify an id and the maximum number of children
// and termination policy when initializing a parallel node
NodeParallel::NodeParallel(byte id, byte max_children, byte policy): 
	NodeInternal(id, max_children)
{
	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	printName(); spf("  max children:"); spl(max_children);
	#endif
	this->_policy = policy;
	
	// Allocate enough bytes so that we can have 1 bit per child
	byte length = (max_children+7)/8;	
	this->_terminated_state = (byte*) malloc(sizeof(byte) * length);
	this->_success_state = (byte*) malloc(sizeof(byte) * length);
	
	// Initialize the terminated and success state of all nodes as 0
	for (byte i=0; i<length; i++)
	{
		this->_terminated_state[i] = 0;
		this->_success_state[i] = 0;
	}
	this->_type = NODE_SEQUENCE;
}


// Destructor
NodeParallel::~NodeParallel()
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
	splf("~NodeParallel"); delay(DEBUG_DELAY);
	#endif
	free(this->_success_state);
	free(this->_terminated_state);
}


// Create a node based of a byte array of data. If the data size
// is invalid to create the desired node, returns null
NodeParallel* NodeParallel::createNode(byte* data, byte data_length)
{
	// Check if the data buffer has the exact amount of data to create the node
	if (!testDataLength(data_length, 4)) return NULL;

	// The first byte contains the node id
	NODE_ID node_id = data[0];
	// The third byte contains the number of children
	byte child_count = data[2];
	// The fourth byte contains the termination policy
	byte policy = data[3];
	
	NodeParallel* node = new NodeParallel( node_id, child_count, policy );
	// Create the new node and add it to the node list
	return node;;	
}


// Go through the children nodes, running them in sequence
NODE_STATE NodeParallel::run()
{
	Node::run();
	
	//splf("NodeParallel::run()");
	// Extract the completion and failure policy values
	bool success_require_all = this->_policy & (byte)NODE_PARALELL_POLICY_COMPLETION;
	bool failure_require_all = this->_policy & (byte)NODE_PARALELL_POLICY_FAILURE;
	
	//spf("success_require_all:"); sp(success_require_all); spf("   failure_require_all:"); spl(failure_require_all);

	// Variables to count the number of successful and failed nodes
	byte success_count = 0;
	byte failure_count = 0;
	
	
	// Now we go through all the children that are still running and run them	
	for (byte child=0; child<this->_max_children; child++)
	{
		//spf("child:"); spl(child);

		// Find the byte that contains the bit we want, and then the bit position
		byte target_byte = child/8;
		byte target_bit = child%8;

		// Extract the bit that indicates if the current child is still running or not
		bool terminated = this->_terminated_state[target_byte] & ( (byte)0x01 << target_bit );

		// If this child is running, keep running it
		if (!terminated)
		{
			//splf("running");

			// Run the child node and get the new running state
			NODE_STATE state = this->_children[child]->run();
			
			switch (state)
			{
				// If the node returned success, mark it as succeded and terminated
				case NODE_STATE_SUCCESS:
					this->_terminated_state[target_byte] |= ( (byte)0x01 << target_bit );
					this->_success_state[target_byte] |= ( (byte)0x01 << target_bit );
					success_count++;
				break;
				// If the node returned success, mark it as failed and terminated
				// (no need to change the _success_state bit, default is failure
				case NODE_STATE_FAILURE:
					this->_terminated_state[target_byte] |= ( (byte)0x01 << target_bit );
					failure_count++;
				break;
			}		
		}
		// This child has already terminated, so we just check the final state of
		// each node to update the success and failure counters
		else
		{			
			//splf("terminated");
			if ( this->_success_state[target_byte] & ( (byte)0x01 << target_bit ) )
				success_count++;
			else failure_count++;			
		}
	}

	//{ spf("success_count:"); sp(success_count); spf("   failure_count:"); spl(failure_count);}

	// After processing all the children nodes, let's see if they all terminated or succeded	
	// Check the termination conditions for failure with require all
	// 


	// Does the node require all children to succeed for it to succeed?
	if (success_require_all)
	{
		// If it does, we need to check if they all succeeded
		if (success_count >= this->_max_children)
		{
			return (this->stop(NODE_STATE_SUCCESS));
		}
	}
	// Does the node require only one child to succeed for it to succeed?
	else
	{
		// If it does, we need to check for a single child
		if (success_count > 0)
		{
			return (this->stop(NODE_STATE_SUCCESS));
		}
	}	


	// Does the node require all children to fail for it to fail?
	if (failure_require_all)
	{
		// If it does, we need to check if they all succeeded
		if (failure_count >= this->_max_children)
		{
			return (this->stop(NODE_STATE_FAILURE));
		}
	}
	// Does the node require only one child to fail for it to fail?
	else
	{
		// If it does, we need to check for a single child
		if (failure_count > 0)
		{
			return (this->stop(NODE_STATE_FAILURE));
		}
	}	

	// If the node didn't succeed or fail, then it's still running
	return NODE_STATE_RUNNING;
}


// Starts the execution of all children nodes
void NodeParallel::start()
{
	NodeInternal::start();
	// Length of the byte array containing the running state of the children
	byte length = (this->_max_children+7)/8;
	
	for (byte i=0; i<length; i++)
	{	
		// Set all the running bits and state bits as 0
		this->_terminated_state[i] = 0x00;
		this->_success_state[i] = 0x00;
	}
}


