/*
Artica CC - http://artica.cc

Gyro node - Tarquinio Mota

V1.0 - 11/01/2016

Base node co control specific Gyto behaviours

This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/
#ifndef ARTICA_TREE_NODE_SERVICE_H
#define ARTICA_TREE_NODE_SERVICE_H

#include "Arduino.h"
#include "Node.h"
#include "ServiceProvider.h"


#include "Utils.h"
#include "DebugConfig.h"


// Tree node that contains a service instance
// A service instance contains all the data necessary to run one specific
// instance of a service. The services are defined in the class ServiceProvider
 
class NodeGyro: public Node
{
	public:
		// A service instance node contains a node id and a pointer to the service provider
		// The data buffer contains different data depending on the node type
		NodeGyro( NODE_ID id, byte* data, byte data_length, ServiceProvider* service_provider );

		// Destructor
		~NodeGyro();

		// Run the service and obtain the current state
		// Most service instances need specific data to run that is 
		// stored in the nodes that reference that service
		virtual NODE_STATE run(){};

	protected:
		// Pointer to the service provider this tree belongs to
		ServiceProvider* _service_provider;
		// Data to configure the service (the data fields depends on the specific service)
		byte* _data;

};

#endif
