/*
Artica CC - http://artica.cc

Behaviour Tree Core - Tarquinio Mota

V0.8 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "BehaviourTree.h"
#include "NodeOverride.h"
#include "NodeSequence.h"
#include "NodeParallel.h"
#include "NodeService.h"

#include "ServiceDelay.h"
#include "ServiceTest.h"
#include "ServiceFunction.h"

#include "Utils.h"
#include "DebugConfig.h"

#include <EEpromStream.h>

// Size of the buffer to load each element when loading the tree
#define LOAD_BUFFER_LENGTH 64

#define HASH_BUFFER_LENGTH 16

// Char codes for each element type when loading the tree
#define LOAD_CODE_TREE 'T'
#define LOAD_CODE_DEVICE 'D'
#define LOAD_CODE_VARIABLE 'V'
#define LOAD_CODE_NODE 'N'
#define LOAD_CODE_CONNECTION 'C'
#define LOAD_CODE_FINISH 'F'


// Maximum number of times the program will request more data after a timeout
#define MAX_RETRY_COUNT 10

// Timeout between data retransmission requests
#define RETRY_TIMEOUT 500


// Default constructor
BehaviourTree::BehaviourTree()
{
	//splf("new BehaviourTree"); delay(DEBUG_DELAY);
	// Create a service provider for the tree
	this->_service_provider = new ServiceProvider();
	this->_complete = false;
	this->_max_nodes = 0;
	this->_nodes = NULL;
	this->_stream = NULL;
}


// Destructor
BehaviourTree::~BehaviourTree()
{
	splf("~BehaviourTree"); delay(DEBUG_DELAY);
	this->reset();
	delete(this->_service_provider);
}


// Initialize all the variables before running a new tree
void BehaviourTree::init(byte max_variables, byte max_nodes, TREE_ID tree_id)
{
	spf("BehaviourTree::init()"); sp(max_variables); sp(':'); spl(max_nodes);
	// Initialize the service provider with the correct number of variables
	this->_service_provider->init(max_variables);

	// Create a buffer for the nodes list with null links
	this->_max_nodes = max_nodes;
	if (this->_max_nodes > 0)
	{
		this->_nodes = (Node**) malloc(sizeof(Node*)*this->_max_nodes);
		for (byte i=0; i<this->_max_nodes; i++) this->_nodes[i] = NULL;
	}

	this->_tree_id = tree_id;

	this->_complete = false;
	this->_root = NULL;
	this->_active = false;
	this->_paused = false;
	this->_stepping = false;

	// This runs when the user uploads a new tree, so it should be different enough every time
	randomSeed(millis());
}


// Reset the tree, freeing all allocated memory from the current tree
void BehaviourTree::reset()
{
	splf("BehaviourTree::reset()"); delay(DEBUG_DELAY);

	spf("this->_max_nodes:"); spl(this->_max_nodes); delay(DEBUG_DELAY);

	// Delete all the nodes (assuming they exist)
	if (this->_max_nodes > 0)
	{
		for (int i=this->_max_nodes-1; i>=0; i--) 
		{
			if (this->_nodes[i] != NULL) 
			{
				#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
				spl(i);
				#endif
				delete(this->_nodes[i]);
				printFreeRam(); delay(DEBUG_DELAY);
			}
		}
	}

	// Reset the service provider
	this->_service_provider->reset();


	// Delete the node array(assuming it exists)
	if (this->_max_nodes > 0)
	{
		free(this->_nodes);
		this->_max_nodes = 0;
		this->_nodes = NULL;
	}

	this->_complete = false;

	this->_tree_id = 0;


	if (this->_stream != NULL)
	{
		this->_stream->print(STRING_TREE_STATE STRING_MESSAGE_SEPARATOR);
		this->_stream->print(this->_tree_id, HEX);
		this->_stream->print(STRING_MESSAGE_SEPARATOR);
		this->_stream->println(getFreeRam());		
	}
}


// Initializes the tree, based on a byte buffer containing the tree
// maximum number of devices and nodes
// Returns false if the data buffer is invalid
bool BehaviourTree::initTree( byte* data, byte data_length)
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
	spf("createTree:"); printArray(data, data_length);
	#endif
	// Make sure the buffer has the correct size to define a tree (4 bytes)
	if (data_length != 4)
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		this->_stream->println(F("Invalid data size to create a tree!"));
		#endif
		return false; 
	}
	// The first byte of the array contains the number of variables
	byte number_variables = data[0];
	// The second byte of the array contains the number of nodes
	byte number_nodes = data[1];

	TREE_ID tree_id = (TREE_ID)data[2] << 8 | (TREE_ID)data[3] ;
	
	// Create the new tree with the specified size
	this->init(number_variables, number_nodes, tree_id);
	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	this->_stream->print(F("Initialized tree with "); this->_stream->print(number_variables); 
	this->_stream->print(F(" variables and ")); this->_stream->print(number_nodes); this->_stream->println(F(" nodes!");
	#endif
	return true;
}


// Adds a new service instance node to the tree node list 
// (this a leaf node that maintains an instance of a service)
// "data" contains only the data relative to the service
// Returns false if the node or node id are invalid
Node* BehaviourTree::createNodeService( byte* data, byte data_length )
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
	spf("createNodeService:"); printArray(data, data_length);
	#endif
	// The first byte of the array contains the node id
	NODE_ID node_id = data[0];
	// The second byte of the array contains the node type
	//NODE_TYPE node_type = data[1];		
	// The third byte of the array contains the service type
	SERVICE_TYPE service_type = data[2];
		
	// Pointer for the service node that will be created
	NodeService* node = NULL;
	// Now we check if the data is valid for each service instance type,
	// and create the respective service usage instance if the data is valid
	switch (service_type)
	{
		case SERVICE_DELAY:
		{
			node = NodeServiceDelay::createNode( data, data_length, this->_service_provider );
		}
		break;

		case SERVICE_TEST:
		{
			node = NodeServiceTest::createNode( data, data_length, this->_service_provider );
		}
		break;

		case SERVICE_FUNCTION:
		{
			node = NodeServiceFunction::createNode( data, data_length, this->_service_provider );
		}
		break;
		// If an unknown node type was received, abort the load
		default:
		{
			#ifdef BEHAVIOUR_TREE_LOG_CREATION
			this->_stream->println(F("Invalid NodeService type!"));
			#endif
		}
		break;
	}
	// Try to add the new node to the tree
	return node;				
}


// Adds a new node to the behaviour tree. "data" contains all 
// the node configuration information encoded in a char array
bool BehaviourTree::addNode(byte* data, byte data_length)
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
	spf("addNode:"); printArray(data, data_length);
	#endif
	// Make sure the buffer has at least enough data to have
	// the node id and the node type
	if (data_length<2)
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		this->_stream->println(F("Not enough data do define a Node!"));
		#endif	
		return false; 
	}
	// Read the generic data for all node types
	// The first byte of the array contains the node id
	byte node_id = data[0];
	// The second byte of the array contains the node type
	byte node_type = data[1];
	
	// Pointer to the node that will be created
	Node* node;

	// Até aqui
	//Free RAM:1811
	//Free RAM:1788

	// Now we check if the data is valid for each node  type,
	// and create the respective node if the data is valid
	switch (node_type)
	{
		case NODE_SEQUENCE:
		{
			node = NodeSequence::createNode( data, data_length );
		}
		break;
		
	// Até aqui
	//Free RAM:1763
	//Free RAM:1740

		case NODE_PARALLEL:
		{
			node = NodeParallel::createNode( data, data_length);
		}
		break;
		case NODE_OVERRIDE:
		{
			node = NodeOverride::createNode( data, data_length);
		}
		break;
		// The node is a service node, let's call another method to parse the service data
		case NODE_SERVICE:
			node = createNodeService(data, data_length);
		break;
		
		// If an unknown node type was received, abort the load
		default:
			#ifdef BEHAVIOUR_TREE_LOG_CREATION
			this->_stream->println(F("Invalid Node type!"));
			#endif
			return false;
		break;
	}
	// Try to add the new node to node list
	return addNode(node_id, node);							
}


// Adds a new connection between two nodes. "data" contains the
// ids of both nodes, the parent is always the first
bool BehaviourTree::addConnection(byte* data, byte data_length)
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
	spf("addConnection:"); printArray(data, data_length);
	#endif
	// Make sure the buffer has exactly enough data for two node ids
	if (data_length != sizeof(NODE_ID)*2)
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		this->_stream->println(F("Invalid data size for a node connection!"));
		#endif	
		return false; 
	}
	
	// Try to get the pointer for both nodes
	NodeInternal* parent = (NodeInternal*)getNode(data[0]);
	Node* child = getNode(data[1]);

	// Check if the parent node exists
	if (parent == NULL) 
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		this->_stream->println(F("Invalid connection, parent does not exist!"));
		#endif	
		return false; 
	}
	// Check if the child node exists
	if (child == NULL) 
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		this->_stream->println(F("Invalid connection, child does not exist!"));
		#endif	
		return false; 
	}
	// Check if the parent node can have children
	if (parent->_type != NODE_SEQUENCE && parent->_type != NODE_PARALLEL && parent->_type != NODE_OVERRIDE )
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		this->_stream->println(F("The parent node is a leaf node!"));
		#endif	
		return false; 
	}
	
	// Everything is ok, so let's add the child node to the parent child list
	parent->addChild(child);
	return true;
}


// Adds a new node to the tree node list
// Returns false if the node or node id are invalid
bool BehaviourTree::addNode(byte id, Node* node)
{
	// Make sure the node has been created
	if (node == NULL)
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		this->_stream->println(F("Cannot create node!"));
		#endif
		return false;
	}
	// Make sure the node has a valid id
	if (id >= this->_max_nodes)
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		this->_stream->print(F("Invalid Node ID:"); this->_stream->println(id);
		#endif
		return false;
	}
	// Add the node to the list
	// Make sure the node has a valid id
	if (this->_nodes[id] != NULL)
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		this->_stream->print(F("Duplicate Node ID:")); spl(id);
		#endif
		return false;
	}
	//spf("------------Adding node:"); spl(id);
	this->_nodes[id] = node;
	
	// If there is no root set yet, set the parameter node as root
	if (this->_root == NULL)
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		this->_stream->println(F("Setting this node as root"));
		#endif
		this->_root = node;
	}
	return true;
}


// Returns a pointer to the node with a specific ID, or null if
// the id does not maych any node. This method can only be called
// while constructing the tree.
Node* BehaviourTree::getNode(NODE_ID id)
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
	//spf("getNode:"); spl(id);
	#endif
	// Make sure the node has a valid id
	if (id >= this->_max_nodes || this->_nodes[id] == NULL)
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		this->_stream->print(F("Node ")); this->_stream->print(id); this->_stream->println(F((" does not exist"));
		#endif
		return NULL;
	}
	return this->_nodes[id];
}


// Adds a new element (device/node/connection) to a tree
// "data" contains information on what the new element is, 
// and the necessary data to initialize it
// Returns false if the element or elementa data are invalid
bool BehaviourTree::processData(byte* buffer, byte buffer_length)
{
	
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
		this->_stream->print(F("processData: ")); this->_stream->print((char)buffer[0]); 
		this->_stream->print(' '); printArray(buffer, buffer_length); this->_stream->println();
	#endif

	//if (tree != NULL ) {splf("1 tree is NOT null");} else  {splf("1 tree is null");}

	//spl(tree->_max_nodes);
	//spl(tree->_service_provider->_max_variables);

	// If the tree was not initialized yet, the first block of data should be the tree dimension
	if (buffer[0] != LOAD_CODE_TREE && this->_max_nodes == 0)
	{
		#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
		this->_stream->println(F("Trying to add elements before creating a tree!"));
		#endif
		return false;
	}


	// Buffer data starting on the second element
	byte* element_buffer = &buffer[1];
	byte element_buffer_length = buffer_length-1;


	// Let's see what the next element is
	switch (buffer[0])
	{
		// The new element contains the dimensions of the tree
		case LOAD_CODE_TREE:
			return this->initTree( element_buffer, element_buffer_length );
		break;

		// The new element is a Node
		case LOAD_CODE_NODE: 
			return this->addNode(element_buffer, element_buffer_length);
		break;

		// The new element is a Variable
		case LOAD_CODE_VARIABLE:
			return this->_service_provider->addVariable( element_buffer, element_buffer_length );
		break;

		
		// The new element is a Device (which are special variables)
		case LOAD_CODE_DEVICE: 
			//splf("adding device!"); delay(DEBUG_DELAY);
			return this->_service_provider->addVariable( element_buffer, element_buffer_length );
			this->_stream->println(F("device added!")); delay(DEBUG_DELAY);
		break;
		// The new element is a connection between two nodes
		case LOAD_CODE_CONNECTION: 
			return this->addConnection(element_buffer, element_buffer_length);
		break;
		// The new element is the "finished loading" mark
		case LOAD_CODE_FINISH:
			// This is the only situation this function ends returning true
			#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
			this->_stream->println(F("Terminator mark received!"));
			#endif
			this->_complete = true;
			return true;
		break;

		
		// If an unknown message was received, abort the load
		default:
			#ifdef BEHAVIOUR_TREE_LOG_CREATION
			this->_stream->println(F("Unknown element type!"));
			#endif
			return false;
		break;
	}

	printFreeRam(); delay(DEBUG_DELAY);

	return false;
}

// Loads and returns a tree from an incoming data stream
// encoding_type specifies if the data is in hexadecimal or binary format
// save specifies if the tree should be saved to the EEprom
// Returns true if the tree has loaded succesefully,
bool BehaviourTree::loadTreeFromStream(Stream* stream, TREE_ENCODING encoding_type, bool save)
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
	this->_stream->println(F("loadTreeFromStream"));
	#endif

	//int debug_cause_error_count = 3;

	stream->setTimeout(RETRY_TIMEOUT);

	// If a previous tree was already loaded, we need to delete it first
	if (this->_complete)
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		stream->println(F("Another tree already in memory, deleting..."));
		#endif
		this->reset();
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		stream->println(F("Deleted!"));
		printFreeRam();
		#endif
	}

	// Now we can start loading the new tree
	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	stream->println(F("Loading new tree..."));
	#endif

	// Char buffer to load the element data encoded in hexadecimal
	char hex_buffer[LOAD_BUFFER_LENGTH];
	// Char buffer to store the data from the previous message, and make sure it's empty
	char hex_buffer_old[LOAD_BUFFER_LENGTH];
	hex_buffer_old[0] = 0;

	// Char buffer to load a hash value for the encoded data
	char hex_hash[HASH_BUFFER_LENGTH];

	// Byte buffer to load the element data encoded in binary
	byte byte_buffer[LOAD_BUFFER_LENGTH];

	// Create a new stream to write to the EEprom, if necessary
	EEpromStream eeprom_stream(1024);

	// Write a 0 on the first position of the stream
	// If the tree loads succesefully, it will be overwritten with a 1
	if (save) eeprom_stream.write((byte)0);

	// Processes data until we're finished
	while (true)
	{
		//splf("load next element");
		// Size of the next block (in hexadecimal encoding)
		byte hex_count = 0;
		// Size of the hash value for the next block (in hexadecimal encoding)
		byte hash_count = 0;
		// Size of the next block (in binary encoding)
		byte byte_count = 0;

		unsigned int my_hash = 0;
		unsigned int msg_hash = 1;

		// We start by reading the next block of data, either in hexadecimal or binary encoding
		if (encoding_type == TREE_ENCODING_HEX)
		{
			// Tries to read the next block of data
			// Data with hexadecimal encoding (coming throuth the serial connection has a hash value
			// after the data, so we read the data and the hash into two separate buffers)
			// We keep trying to read the data untill the received hash matches the one calculated here
			while (my_hash != msg_hash)
			{
				hex_count = stream->readBytesUntil('|', hex_buffer, LOAD_BUFFER_LENGTH-1);
				// Add a null terminator to the buffer
				hex_buffer[hex_count] = 0;

				// Only advance to reading the hash if the message had data
				if ( hex_count>0 )
				{
					hash_count = stream->readBytesUntil(';', hex_hash, HASH_BUFFER_LENGTH-1);
					hex_hash[hash_count] = 0;

					my_hash = BehaviourTree::calculateHash(hex_buffer);
					msg_hash = atoi(hex_hash);
				}

				//debug_cause_error_count --;
				
				//spf("my_hash:"); spl(my_hash);
				//spf("msg_hash:"); spl(msg_hash);
				//spf("debug_cause_error_count:"); spl(debug_cause_error_count);

				// If the received and calculated hash match, send a confirmation back to the server
				if ( my_hash == msg_hash)
				{
					stream->print(F("RX2,"));
					stream->print(my_hash);				
					stream->println(';');
				}
				else
				{
					stream->println(F("FAIL;"));
					this->_transmit_retry_count++;
					if (this->_transmit_retry_count == MAX_RETRY_COUNT) return false;
				}
			}

			// If we receive the same data wtice in a row, we won't process it
			// This can happen when the data is received fine the first time, but 
			//the server doesn't receive the confirmation, so it sends the data again

			
			//spf("old:"); spl(hex_buffer_old);
			//spf("new:"); spl(hex_buffer);
			
			if (strcmp(hex_buffer_old, hex_buffer) == 0)
			{
				stream->println(F("############### REPEATED DATA!!"));
			}

			if (strcmp(hex_buffer_old, hex_buffer) == 0) continue;

			// Now we wait for the next block of data, which should be a message saying the reply was succeceful
			//hex_count = stream->readBytesUntil(';', hex_buffer, LOAD_BUFFER_LENGTH-1);

		}	
		else			
		{
			// Tries to read the next block of data
			byte_count = stream->readBytesUntil(';', (char*)byte_buffer, LOAD_BUFFER_LENGTH-1);
		}	

		// If both buffers are empty, it means no data was read
		if ( hex_count == 0 && byte_count == 0)
		{
			#ifdef BEHAVIOUR_TREE_LOG_CREATION
			stream->println(F("Null data block!");
			#endif	
			// If the tree already was initialized/had elements, we need to delete it
			this->reset();
			return false;
		}

		
		// If the stream is encoded in hexadecimal, we need to convert it to binary before processing
		if (encoding_type == TREE_ENCODING_HEX)
		{
			// The first byte (the element identifier) remains unchanged
			byte_buffer[0] = hex_buffer[0];
			// For the rest of the bytes, convert the ascii hex representation to a byte value
			byte_count = 1;
			for (byte i=1; i<hex_count; i+=2)
			{
				byte_buffer[byte_count++] = parseInt8FromArray(hex_buffer, i);
			}
		}

		if (save) 
		{
			#ifdef BEHAVIOUR_TREE_DEGUG_CREATION
			stream->print(F("Saving to EEprom:\"")); stream->print(hex_buffer); stream->println('\"');
			#endif
			for (byte i=0; i<byte_count; i++) eeprom_stream.write(byte_buffer[i]);
			eeprom_stream.write(';');
		}


		// Try to add the new element to the tree (or create the tree if we're just starting)
		if (!this->processData(byte_buffer, byte_count))
		{
			#ifdef BEHAVIOUR_TREE_LOG_CREATION
			stream->println(F("Error adding element to the tree!"));
			stream->println(F("ERROR;"));
			#endif	
			// If the tree already was initialized/had elements, we need to delete it
			this->reset();
			return false;
		}


		// Is the tree loading has finished, flush the rest of the incoming stream data
		if (this->_complete)
		{
			// Flush the data from the incoming buffer
			while (stream->available()) { stream->read(); if (!stream->available()) delay(5); }
			#ifdef BEHAVIOUR_TREE_LOG_CREATION
			stream->println(F("Loading tree complete!"));
			#endif
			// If we're saving to the eeprom, set the first byet at 1, to mark the tree as valid
			if (save)
			{
				#ifdef BEHAVIOUR_TREE_LOG_CREATION
				stream->print(F("EEprom used:")); stream->print(100*eeprom_stream.getWritePointer() / eeprom_stream.getSize()); stream->println('%');
				#endif

				eeprom_stream.setWritePointer(0);
				eeprom_stream.write((byte)1);
			}
			// Start running the tree immediatlly
			this->start();
			return true;
		}

		// Save the data of this message, so we can make sure we don't process the same data twice
		strcpy(hex_buffer_old, hex_buffer);
	}

	return false;
}


// Loads a new tree from the internal EEprom memory
// Returns true if a valid tree was loaded
bool BehaviourTree::loadTreeFromEEprom()
{
	//splf("loadTreeFromEEprom");
	
	// Create a eepron stream object to read data from the eeprom with the Stream interface
	EEpromStream eeprom_stream(1024);
	// The first byte of the eeprom inficates if we should try to read a tree or not
	byte load = eeprom_stream.read();
	
	// Até aqui
	//Free RAM:1811
	//Free RAM:1788

	//spf("eeprom:");spl(load);
	if (load)
	{

 		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		stream->println(F("Loading tree from the EEprom"));
		#endif
		return this->loadTreeFromStream(&eeprom_stream, TREE_ENCODING_BINARY, false);
	}
	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	stream->println(F("Nothing to load from the EEprom"));
	#endif
	return false;
}


// Deletes any tree from the internal EEprom memory
void BehaviourTree::deleteTreeFromEEprom()
{
	// The first byte of the eeprom indicates if we should try to read a tree or not,
	// so we set it to 0
	EEPROM.write(0, 0);
	// Also delete the tree from RAM
	this->reset();
}


// Run the BehaviourTree
// This should be called often of the loop() to ensure the tree runs propperly
NODE_STATE BehaviourTree::run()
{
	//spf("run tree: "); splvar(this->_complete); delay(100);

	// Check if there is any incoming data on the assigned stream
	if (this->_stream != NULL && this->_stream->available()) parseCommand();


	// If the tree is not complete, nothing happens
	if (!this->_complete) return NODE_STATE_INACTIVE;


	// Otherwise let's run the next frame of the tree and set the new state
	NODE_STATE state = this->runNextFrame();

	// Report the nodes that changed state on every frame
	this->report();


	// Stays on this loop untill the desired time for a loop has passed
	while (micros() - this->_time_last_loop < (unsigned long)LOOP_DELAY*1000);
	this->_time_last_loop = micros();

	// If the tree is not set to run, return inactive state
	return state;
}


// Run the next frame of the tree
NODE_STATE BehaviourTree::runNextFrame()
{
	// Otherwise let's run the tree and save the new state
	NODE_STATE state = NODE_STATE_INACTIVE;

	// Runs the next frame of the tree only if the tree is active and 
	// not paused, or if we gave the order to advance one step
	if ( ( this->_active && !this->_paused) || this->_stepping)
	{
		// If we're stepping, only run one step at a time
		this->_stepping = false;

		// Read new data from all the inputs
		this->_service_provider->readInputs();
		
		//splf("run le root!"); delay(100);

		// Run the next frame of the tree
		this->_root->run();

		//splf("done!"); delay(100);

		// Write new processed to all the outputs
		this->_service_provider->writeOutputs();
		state = NODE_STATE_RUNNING;
	}

	// If the tree is not set to run, return inactive state
	return state;
}



// Methods to control the running state of the tree
// Starts executing the tree. Has no effect if the tree is already running
void BehaviourTree::start()
{
	// If the tree is not complete, nothing happens
	if (!this->_complete) return;
	this->_active = true;
	this->_paused = false;
}

// Pauses the execution of the tree. After a pause(), start() and step()
// will continue the execution from the same point where the pause happend
void BehaviourTree::pause()
{
	// If the tree is not complete, nothing happens
	if (!this->_complete) return;
	this->_paused = true;
}

// Stops the execution of the tree. After a stop, calling start() or step()
// will start executing the tree from the beginning
void BehaviourTree::stop()
{
	// If the tree is not complete, nothing happens
	if (!this->_complete) return;

	// If the tree is complete, we need to stop all the nodes
	this->_active = false;
	this->_root->stop(NODE_STATE_INACTIVE);

	// We also set all the output devices to their default state
	this->_service_provider->resetDevices();
}

// Runs a single step of the tree, continuing from the last position	
// If the tree is running, it will be paused after this	
void BehaviourTree::step()
{
	// If the tree is not complete, nothing happens
	if (!this->_complete) return;
	this->_active = true;
	this->_paused = true;
	this->_stepping = true;
}


// Reports the nodes that have changed in the last iteration running the tree
// if full_report is true it dumps the status of all the nodes
void BehaviourTree::report()
{
	// Let's check if any of the nodes changed state since the last frame
	bool nodes_changed = false;
	for (byte i=0; i<this->_max_nodes; i++) 
	{
		Node* node = this->_nodes[i];
		// Check is the the state of the node changed since the last frame
		if ( node->_old_state != node->_state)
		{
			nodes_changed = true;
			// Save the current state as the old state (for the next frame)
			node->_old_state = node->_state;
		}
	}
	// If at least one of the nodes changed value, print the state of all the nodes
	if (nodes_changed) 
	{
		this->_stream->print(STRING_TREE_STATE STRING_MESSAGE_SEPARATOR);
		this->_stream->print(this->_tree_id, HEX);
		this->_stream->print(STRING_MESSAGE_SEPARATOR);
		this->_stream->print(getFreeRam());
		this->_stream->print(STRING_MESSAGE_SEPARATOR);

		for (byte i=0; i<this->_max_nodes; i++) 
		{
			Node* node = this->_nodes[i];
			this->_stream->print(node->_state);
		}
		this->_stream->println();
	}

	// Now let's check if any of the variables changed value since the last frame
	bool variables_changed = false;
	for (byte i=0; i<this->_service_provider->_max_variables; i++) 
	{
		Variable* variable = this->_service_provider->_variables[i];
		// Check is the the state of the node changed since the last frame
		if ( variable->changed())
		{
			variables_changed = true;
		}
	}
	// If at least one of the variables changed value, print the value of all the variables
	if (variables_changed) 
	{
		this->_stream->print(STRING_VARIABLES_STATE STRING_MESSAGE_SEPARATOR);
		this->_stream->print(this->_tree_id, HEX);
		this->_stream->print(STRING_MESSAGE_SEPARATOR);

		for (byte i=0; i<this->_service_provider->_max_variables; i++) 
		{
			Variable* variable = this->_service_provider->_variables[i];
			this->_stream->print(variable->read());
			this->_stream->print(PARAMETER_MESSAGE_SEPARATOR);
		}
		this->_stream->println();
	}

}


// Calculate a hash value (unsigned int) for a string
unsigned int BehaviourTree::calculateHash(char* str)
{
	unsigned int hash = 0;
	byte  i, len;
	len = strlen(str);
	if (len == 0) return hash;
	for (i = 0; i<len; i++) 
	{
		unsigned int mask = str[i] << ((str[i]*7+i)%8);
		hash  = hash ^ mask;
	}
	return hash;
}


// Set the stream which is going to control the tree
void BehaviourTree::setStream(Stream* stream)
{
	this->_stream = stream;
}


// Flushes all the data from an incoming data stream
void BehaviourTree::flushStream()
{
	//splf("flushing..."); delay(DEBUG_DELAY);
	if (this->_stream == NULL) return;

	while (this->_stream->available()) 
	{ 
		this->_stream->read(); 
		if (!this->_stream->available()) delay(10); 
	}

	//splf("flushed!"); delay(DEBUG_DELAY);
}


// Parse the commands from the incoming data strean
void BehaviourTree::parseCommand()
{
	// Char buffer to load the element data encoded in hexadecimal
	char hex_buffer[32];
	// Char buffer to load a hash value for the encoded data
	char hex_hash[16];
	// Size of the next block (in hexadecimal encoding)
	byte hex_count = 0;
	// Size of the hash value for the next block (in hexadecimal encoding)
	byte hash_count = 0;

	hex_count = this->_stream->readBytesUntil('|', hex_buffer, 50);
	hex_buffer[hex_count] = 0;

	// @todo is this necessary?
	if (false)
	{
		this->flushStream();
		return;
	}

	hash_count = this->_stream->readBytesUntil(';', hex_hash, 50);
	hex_hash[hash_count] = 0;

	spf("command:"); spl(hex_buffer);

	unsigned int my_hash = BehaviourTree::calculateHash(hex_buffer);
	unsigned int msg_hash = atoi(hex_hash);


	// Read from the stream untill we find the mark that separates the command from the hash
	//this->_stream->readBytesUntil(COMMAND_START_HASH, hash_buffer, LOAD_BUFFER_LENGTH-1);


	//spf("I'm here!");
	// Sends the data back to the server so we're sure it was received succecefuly
	this->_stream->print(F("RX1,"));
	this->_stream->print(my_hash);
	this->_stream->println(';');


	printFreeRam();
	switch (hex_buffer[0])
	{
		case COMMAND_LOAD_TREE: this->loadTreeFromStream(this->_stream, TREE_ENCODING_HEX, false); break;
		case COMMAND_SAVE_TREE: this->loadTreeFromStream(this->_stream, TREE_ENCODING_HEX, true); break;
		case COMMAND_DELETE_TREE: this->deleteTreeFromEEprom(); break;
		case COMMAND_RUN_TREE: this->start(); break;
		case COMMAND_STOP_TREE: this->stop(); break;
		case COMMAND_PAUSE_TREE: this->pause(); break;
		case COMMAND_STEP_TREE: this->step(); break;
	}

	this->flushStream();
}


