/*
Artica CC - http://artica.cc

Function service support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "ServiceFunction.h"


// The delay service node contains the node ID and the delay duration
NodeServiceFunction::NodeServiceFunction( NODE_ID id, byte* data, byte data_length, ServiceProvider* service_provider ):
	NodeService(id, data, data_length, service_provider)
{
	// Get the type of function to initialize
	byte function = this->_data[0];
	
	// The first position of the data is the test type, so start reading the rest from pos 1
	byte pos = 1;

	// 
	switch (function)
	{
		// The function is a RANGE
		case FUNCTION_TYPE_RANGE:
		{
			// We need to initialize the range direction and initial values
			long min_value = this->_service_provider->getValueFromBuffer(this->_data, &pos);
			this->_range_direction = FUNCTION_RANGE_DIRECTION_UP;
			this->_range_value = min_value;
			splf("Initializing RANGE function");
			splvar(this->_range_direction);
			splvar(this->_range_value);
		}
	}

}


// Destructor
NodeServiceFunction::~NodeServiceFunction()
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
	splf("~NodeServiceFunction");
	#endif
} 


// Create a node based of a byte array of data. If the data size
// is invalid to create the desired node, returns null
NodeServiceFunction* NodeServiceFunction::createNode(byte* data, byte data_length, ServiceProvider* service_provider)
{
	// Test nodes have different data lenghts depending on the test type
	// and the data types involved in the test
	byte node_data_length = data[3];

	// Check if the data buffer has the exact amount of data to create the node
	if (!testDataLength(data_length, node_data_length+4)) return NULL;

	// The first byte of the array contains the node id
	NODE_ID node_id = data[0];
	
	NodeServiceFunction* node = new NodeServiceFunction(node_id, &(data[4]), node_data_length, service_provider);
	// At this point everything should be fine, so let's create the node
	return node;
}


// Returns NODE_STATE_SUCCESS if enough time has passed since the 
// delay started, NODE_STATE_RUNNING otherwise
NODE_STATE NodeServiceFunction::run()
{
	Node::run();

	// Get the type of function to execute
	byte function = this->_data[0];
	
	long output_value = 0;
	// The first position of the data is the test type, so start reading the rest from pos 1
	byte pos = 1;

	switch (function)
	{
		// The function is a COPY
		case FUNCTION_TYPE_COPY:
		{
			// In this case the optput value is simply a copy of the input value
			output_value = this->_service_provider->getValueFromBuffer(this->_data, &pos);
		}
		break;
		// The function is a MAP
		case FUNCTION_TYPE_MAP:
		{
			// Read all the map variables and pass them to the arduino map() function						
			long in_value = this->_service_provider->getValueFromBuffer(this->_data, &pos);
			long in_min  = this->_service_provider->getValueFromBuffer(this->_data, &pos);
			long in_max = this->_service_provider->getValueFromBuffer(this->_data, &pos);
			long out_min  = this->_service_provider->getValueFromBuffer(this->_data, &pos);
			long out_max = this->_service_provider->getValueFromBuffer(this->_data, &pos);
			output_value = map(in_value, in_min, in_max, out_min, out_max );
		}
		break;
		// The function is a RANDOM
		case FUNCTION_TYPE_RANDOM:
		{
			// Read all the min and max values and pass them to the arduino random() function
			// randomSeed() is executes whenever the user uploads a new tree
			long min_value = this->_service_provider->getValueFromBuffer(this->_data, &pos);
			long max_value  = this->_service_provider->getValueFromBuffer(this->_data, &pos);
			// @togo more randomSeeds in different places? reading a tree from the eeprom
			// should have always the same randomseed... that's bad!
			output_value = random(min_value, max_value);
			//splvar(output_value);
		}
		break;
		// The function is a RANGE
		case FUNCTION_TYPE_RANGE:
		{
			long min_value = this->_service_provider->getValueFromBuffer(this->_data, &pos);
			long step_value  = this->_service_provider->getValueFromBuffer(this->_data, &pos);
			long max_value  = this->_service_provider->getValueFromBuffer(this->_data, &pos);

			// Now we change the range value depending on the direction
			if (this->_range_direction == FUNCTION_RANGE_DIRECTION_UP)
			{
				this->_range_value += step_value;

				//splvar(max_value);
				// Check if the value has passed the upper bound
				if (this->_range_value >= max_value)
				{
					// Change direction and constrain the value to the max_value
					this->_range_direction = FUNCTION_RANGE_DIRECTION_DOWN;
					this->_range_value = max_value;
				}
			}
			// If it's not going up, it's going down!
			else
			{
				this->_range_value -= step_value;

				// Check in the value has passed the lower bound
				if (this->_range_value <= min_value)
				{
					// Change direction and constrain the value to the min_value
					this->_range_direction = FUNCTION_RANGE_DIRECTION_UP;
					this->_range_value = min_value;
				}
			}

			// We set the output value to the calculated range value
			output_value = this->_range_value;
			//splvar(this->_range_value);
			//splvar(this->_range_direction);
		}
		break;
	}


	//splvar(output_value);

	// The last data in the data buffer is always the target to change
	this->_service_provider->changeVariableValue(output_value, this->_data, &pos);


	// If not enough time has passed, return running state
	this->stop(NODE_STATE_SUCCESS);
	return NODE_STATE_SUCCESS;
}

/*
// Initialize any functions that need to mantain state between iterations
void NodeServiceFunction::start()
{
	Node::start();


	// Get the type of function to initialize
	byte function = this->_data[0];
	
	// The first position of the data is the test type, so start reading the rest from pos 1
	byte pos = 1;

	switch (function)
	{
		// The function is a RANGE
		case FUNCTION_TYPE_RANGE:
		{

			// We need to initialize the range direction and initial values
			long min_value = this->_service_provider->getValueFromBuffer(this->_data, &pos);
			this->_range_direction = FUNCTION_RANGE_DIRECTION_UP;
			this->_range_value = min_value;
			splf("Initializing RANGE function");
			splvar(this->_range_direction);
			splvar(this->_range_value);
		}
	}
}
*/





