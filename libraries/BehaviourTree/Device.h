/*
Artica CC - http://artica.cc

Generic device type - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef ARTICA_DEVICE_H
#define ARTICA_DEVICE_H

#include "Arduino.h"

#include "Utils.h"
#include "DebugConfig.h"

// Possible results when trying to initialize a device
enum DEVICE_INITIALIZATION
{
  DEVICE_INITIALIZATION_SUCCESSFUL,
  DEVICE_INITIALIZATION_FAILURE
};


// Set the data type used for device ids
typedef byte DEVICE_TYPE;


// Generic class to represent a device
class Device
{
	public:
		// Default constructor
		Device();
		
		// Initialize the device and obtain the initialization state
		virtual DEVICE_INITIALIZATION initialize(){};
		 
		// Returns the device type for each different service
		virtual DEVICE_TYPE getDeviceType(){};
		
		// Prints the device class name
		void printName() { spf("Device"); };

};

#endif
