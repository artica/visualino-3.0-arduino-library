/*
Artica CC - http://artica.cc

Parallel node - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef ARTICA_TREE_NODE_PARALELL_H
#define ARTICA_TREE_NODE_PARALELL_H

#include "Arduino.h"
#include "NodeInternal.h"

#include "Utils.h"
#include "DebugConfig.h"


// Tree node that contains a sequence of children nodes
// The children nodes will run in a specific order, untill the 
// last one succeeds, or one of them fails

class NodeParallel: public NodeInternal
{ 
	public:
		// We need to specify an id and the maximum number of children
		// when initializing a paralel node
		NodeParallel(byte id, byte max_children, byte policy);
		// Destructor
		~NodeParallel();
		
		// Create a node based of a byte array of data. If the data size
		// is invalid to create the desired node, returns null
		static NodeParallel* createNode(byte* data, byte data_length);

		// Go through the children nodes, running them all
		NODE_STATE run();

		// Starts the execution of all children nodes
		void start();

		// Stops the execution of all children nodes
		//NODE_STATE stop(NODE_STATE state);

		// Prints the node class name
		void printName() { spf("NodeParallel"); };

	private:
		// Running policy for the node
		byte _policy;
		// Byte buffer containing information about which children
		// nodes are still running (1 bit per child)
		byte* _terminated_state;
		// Byte buffer containing information about the success state
		// of the children that have stopped running (1 bit per child)
		byte* _success_state;
};

#endif
