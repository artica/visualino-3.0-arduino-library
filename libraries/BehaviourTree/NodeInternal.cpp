/*
Artica CC - http://artica.cc

Internal tree node - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "NodeInternal.h"

#include "Utils.h"
#include "DebugConfig.h"

// We need to specify an id and the maximum number of children
// when initializing an internal node
NodeInternal::NodeInternal(byte id, byte max_children): 
	Node(id)
{
	this->_children = (Node**) malloc (sizeof(Node*) * max_children);
	this->_current_child = 0;
	this->_max_children = max_children;
}

// Destructor
NodeInternal::~NodeInternal()
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
	splf("~NodeInternal"); delay(DEBUG_DELAY);
	#endif
	free(this->_children);
} 

// Add the next child to this node
void NodeInternal::addChild(Node* child)
{
	// Make sure the node isn't full already
	if (this->_current_child >= this->_max_children)
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		spf("Trying to add a child to a full node: "); spl(this->_id);
		#endif
		return;
	}
	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	spf("Adding connection: "); spx(this->_id); spf(" -> "); spx(child->_id); spl();
	#endif
	// Add the received node pointer to the list of this nodes children
	this->_children[this->_current_child++] = child;
}

// Runs the node, returning the node state
NODE_STATE NodeInternal::run()
{
	//spf("Run NodeInternal:"); spl(this->_id);
	return NODE_STATE_RUNNING;
}


// Start running an internal node
void NodeInternal::start()
{
	Node::start();
	// When we start running an internal node, we make sure all the children nodes are inactive
	for (byte child=0; child<this->_max_children; child++)
	{
		// Only affects the nodes that aren't already inactive
		if (this->_children[child]->_state != NODE_STATE_INACTIVE) this->_children[child]->stop(NODE_STATE_INACTIVE);
	}
	// Sets the next child node to be processed as the first node
	this->_current_child = 0;
}

// Stops the execution of all children nodes
NODE_STATE NodeInternal::stop(NODE_STATE state)
{
	Node::stop(state);

	// If we just set this node as inactive, set all the children nodes as inactive too
	if (state == NODE_STATE_INACTIVE) for (byte child=0; child<this->_max_children; child++)
	{
		// Stop the next child node
		if (this->_children[child]->_state != NODE_STATE_INACTIVE) this->_children[child]->stop(NODE_STATE_INACTIVE);
	}
	return state;
}
