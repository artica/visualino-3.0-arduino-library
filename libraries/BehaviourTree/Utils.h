#ifndef ARTICA_GENERIC_UTILS_H
#define ARTICA_GENERIC_UTILS_H

// Small generic utility functions
#include "Arduino.h"

#include "Utils.h"
#include "DebugConfig.h"

#define DEBUG_DELAY 10

// Macros to shorten the print calls
#define sp(text) Serial.print(text)
#define spl(text) Serial.println(text)
#define spf(text) Serial.print(F(text))
#define splf(text) Serial.println(F(text))

#define spx(text) Serial.print(text, HEX)
#define spxl(text) Serial.println(text, HEX)
#define spd(text) Serial.print(text, DEC)
#define spdl(text) Serial.println(text, DEC)
#define spvar(var) { Serial.print(F(#var"=")); Serial.print(var); Serial.print(F("  ")); }
#define splvar(var) { Serial.print(F(#var"=")); Serial.println(var); }


// Macros used to simplify the declaration of variables and prints
// Use: P(my_var) = "Hello";
#define P(name)   static const char name[] PROGMEM


// Convert a hexadecimal character to a numerical value
byte hex2byte(char hex);

// Parse two chars of a byte array as a hexadecimal number
byte parseHexByte(char* buffer, byte pos);

// Parse eight chars of a byte array as a hexadecimal number
unsigned long parseHexULong(char* buffer, byte pos);

// Prints as array of bytes as numerical values
void printArray(byte* array, byte length);

// Extract an int variable from a specific position in a byte array
int getIntFromArray(byte* data, byte pos);

// Extract an unsigned long variable from a specific position in a byte array
unsigned long getUnsignedLongFromArray(byte* data, byte pos);


// Start a timer to measure the time (in microseconds);
void startTimer();
// Print how much time has passed since startTimer() was called
void checkTimer();


#endif
