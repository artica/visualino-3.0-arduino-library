/*
Artica CC - http://artica.cc

Service Provider for a Tree - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "ServiceProvider.h"

#include "DeviceOutputDigitalPin.h"
#include "DeviceOutputAnalogPin.h"
#include "DeviceInputDigitalPin.h"
#include "DeviceInputAnalogPin.h"
#include "DeviceOutputMotor.h"

#ifdef GYRO_ROBOT_SUPPORT
	#include "DeviceGyroMotor.h"
	#include "DeviceGyroDistanceSensor.h"
	#include "DeviceGyroLightSensor.h"
#endif


#include "VariableBool.h"
#include "VariableByte.h"
#include "VariableInt.h"
#include "VariableLong.h"


#include "Utils.h"
#include "DebugConfig.h"

// When creating the service provider we need to specify the maximum 
// number of variables this service provider can handle
ServiceProvider::ServiceProvider()
{
	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	splf("new ServiceProvider");
	#endif

	#ifdef GYRO_ROBOT_SUPPORT
	
	    splf("-------gyro init started");
		// gyro.motoruino2 Configuration
		this->_gyro.motoruino2.config_imu(true, true, true);
		this->_gyro.motoruino2.config_motor(M1_SIGNAL,M2_SIGNAL);
		this->_gyro.motoruino2.config_power();

		this->_gyro.motoruino2.setSpeedPWM(0,0);

	    if (this->_gyro.motoruino2.calibrateGyro())
	        Serial.println(F("Calibrated OK"));
	    else
	        Serial.println(F("Not Calibrated"));

	    this->_gyro.motoruino2.resetGyro(true,true,true);


		this->_gyro.bumpers.begin(SENSOR_PROXIMITY, SENSOR_PROXIMITY, SENSOR_PROXIMITY);

	    splf("-------gyro init complete");
		    
	#endif

	this->_max_variables = 0;
}

// Destructor
ServiceProvider::~ServiceProvider()
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
	splf("~ServiceProvider"); delay(DEBUG_DELAY);
	#endif
	this->reset();
}


// Initialize the ServiceProvider for a specific number of variables
void ServiceProvider::init(byte max_variables)
{
	spf("Init ServiceProvider: max_variables:"); spl(max_variables);
	this->_max_variables = max_variables;
	// Create a buffer for the variables list with null links
	if (this->_max_variables > 0)
	{
		spf("gonna malloc():"); spl(sizeof(Variable*) * this->_max_variables); delay(DEBUG_DELAY);
		this->_variables = (Variable**) malloc(sizeof(Variable*) * this->_max_variables );
		splf("malloc() done"); delay(DEBUG_DELAY);

		for (byte i=0; i<this->_max_variables; i++)
		{
			spf("gonna nullify:"); spl(i); delay(DEBUG_DELAY);
			this->_variables[i] = NULL;		
		}
	}
	splf("Init ServiceProvider complete");
}


// Reset the ServiceProvider, freeing all allocated memory for variables
void ServiceProvider::reset()
{
	splf("ServiceProvider::reset()"); delay(DEBUG_DELAY);
	// Delete all the variables
	if (this->_max_variables > 0)
	{
		for (int i=this->_max_variables-1; i>=0; i--) 
		{
			if (this->_variables[i] != NULL) 
			{
				#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
				spf("deleting variable:");  spl(i); delay(DEBUG_DELAY);
				#endif
				delete(this->_variables[i]);
				#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
					printFreeRam(); delay(DEBUG_DELAY);
				#endif
			}
		}
		splf("deleting _variables array"); delay(DEBUG_DELAY);
		// Clear the memory from the variable array
		free(this->_variables);
	}
	this->_max_variables = 0;
}


// Adds a new variable to the service provider
// data is binary encoded data used to initialize the variable
// The first byte of data is the variable ID, the rest of the
// data will depend on the variable type
// Returns false if there is any error with the variable data
bool ServiceProvider::addVariable(byte* data, byte data_length)
{
	//spf("###addVariable: "); printArray(data, data_length); delay(DEBUG_DELAY);
	// Make sure the buffer has at least enough data to have
	// the variable id and the variable type
	if (data_length<2)
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		splf("Not enough data to define a Variable!");
		#endif	
		return false; 
	}
	// Read the generic data for all variable types
	// The first byte of the array contains the variable id
	VARIABLE_ID variable_id = data[0];	
	
	// The second byte of the array contains the variable type
	byte variable_type = data[1];
	
	//spf("variable_type:"); spl(variable_type); 
	
	// Pointer for the variable that will be created
	Variable* variable;
	// Create the desired variable
	switch (variable_type)
	{
		case VARIABLE_DATA_BOOL:
		{
			variable = VariableBool::createVariable( data, data_length );			
		}
		break;
		case VARIABLE_DATA_BYTE:
		{
			variable = VariableByte::createVariable( data, data_length );			
		}
		break;
		case VARIABLE_DATA_INT:
		{
			variable = VariableInt::createVariable( data, data_length );			
		}
		break;
		case VARIABLE_DATA_LONG:
		{
			variable = VariableLong::createVariable( data, data_length );			
		}
		break;
		case VARIABLE_OUTPUT_DIGITAL_PIN:
		{
			variable = DeviceOutputDigitalPin::createDevice( data, data_length );			
		}
		break;
		case VARIABLE_OUTPUT_ANALOG_PIN:
		{
			variable = DeviceOutputAnalogPin::createDevice( data, data_length );			
		}
		break;
		case VARIABLE_INPUT_DIGITAL_PIN:
		{
			variable = DeviceInputDigitalPin::createDevice( data, data_length );			
		}
		break;
		case VARIABLE_INPUT_ANALOG_PIN:
		{
			variable = DeviceInputAnalogPin::createDevice( data, data_length );			
		}
		break;
		case VARIABLE_OUTPUT_MOTOR:
		{
			variable = DeviceOutputMotor::createDevice( data, data_length );			
		}
		break;
		#ifdef GYRO_ROBOT_SUPPORT
		case VARIABLE_GYRO_OUTPUT_MOTOR:
		{
			variable = DeviceGyroMotor::createDevice( data, data_length, this);			
		}
		break;
		case VARIABLE_GYRO_INPUT_DISTANCE_SENSOR:
		{
			variable = DeviceGyroDistanceSensor::createDevice( data, data_length, this);			
		}
		break;
		case VARIABLE_GYRO_INPUT_LIGHT_SENSOR:
		{
			variable = DeviceGyroLightSensor::createDevice( data, data_length, this);			
		}
		break;
		#endif
		// If an unknown variable type was received, abort the load
		default:
			#ifdef BEHAVIOUR_TREE_LOG_CREATION
			splf("Unknown variable type!");
			#endif
			return false;
		break;
	}
	//splf("adding variable...");
	// Try to add the new variable to the list
	return addVariable(variable_id, variable);				
}


// Adds a new variable to the service provider
// Returns false if the variable or variable id are invalid
bool ServiceProvider::addVariable(VARIABLE_ID id, Variable* variable)
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION	
	spf("FDX addVariable:"); spl(id);
	#endif
	// Make sure the variable has been created
	if (variable == NULL)
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		splf("Cannot create variable!");
		#endif
		return false;
	}
	// Make sure the variable has a valid id
	if (id >= this->_max_variables)
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		spf("Invalid Variable ID:"); spl(id);
		#endif
		return false;
	}
	// Make sure we there isn't any other variable with the same id
	if (this->_variables[id] != NULL)
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		spf("Duplicate Variable ID:"); spl(id);
		#endif
		return false;
	}	
	//splf("Success!"); delay(DEBUG_DELAY);
	// Add the variable to the list
	this->_variables[id] = variable;
	return true;
}

// Get a numerical value from a data source
// Data sources can be constants, variables or devices
// The "pos" position of the buffer should be a header byte, 
// identifying the data source and data type for the value
long ServiceProvider::getValueFromBuffer(byte* buffer, byte* pos)
{
	// The header is the first byte of the byte buffer
	byte header = buffer[*pos];
	//splvar(header);

	// Extract the data source from the header byte
	byte source = header >> 6;

	////splvar(source);

	byte type = (header & B00111000) >> 3;
	//splvar(type);

	long result = 0;

	switch (source)
	{
		case DATA_SOURCE_TYPE_CONSTANT:
			//splf("it's a constant!");

			switch(type)
			{
				// If the data type is boolean, the value is in the first bit of the header byte
				case VARIABLE_DATA_BOOL:
					//splf("bool");
					result = (long)(header & B00000001);
				break;
				// if the data type is byte, the value is the byte just after the header
				case VARIABLE_DATA_BYTE:
					//splf("byte");
					result = (long)(buffer[++(*pos)]);
				break;
				case VARIABLE_DATA_INT:
					result = (long)buffer[(*pos)+1] << 8 | (long)buffer[(*pos)+2] ;

					//splvar(result);


					//N030302060010ff008003
					
					// result = 65280

					if (result>32767) result = result-65535;

					// result = 65280

					(*pos) += 2;
				break;
				case VARIABLE_DATA_LONG:
					splf("long");
					result = result | (long)buffer[(*pos)+1] << 24 | (long)buffer[(*pos)+2] << 16 | (long)buffer[(*pos)+3] << 8 | (long)buffer[(*pos)+4];

					//splvar(result);

					// @todo this conversion should be better... :s
					if (result>2147483647) result = result-4294967295;
					if (result<0) result++;


					//splvar(result);


					(*pos) += 4;
				break;
			}
		break;
		case DATA_SOURCE_TYPE_VARIABLE:
		case DATA_SOURCE_TYPE_DEVICE:
		{
			//splf("it's a variable!");
			byte var = (long)buffer[++(*pos)];
			//spl(var);
			result = this->getVariableValue(var);
		}
	}

	// Advance to the next position (should put the pointer at the beginning of the next variable, if it exists)
	(*pos)++;
	return result;
}


// Sets a new value to a variable identified in a byte buffer
void ServiceProvider::changeVariableValue(long value, byte* buffer, byte* pos)
{
	// The header is the first byte of the byte buffer
	byte header = buffer[*pos];

	// Extract the data source from the header byte
	byte source = header >> 6;

	switch (source)
	{
		case DATA_SOURCE_TYPE_VARIABLE:
		case DATA_SOURCE_TYPE_DEVICE:
		{
			//splf("it's a variable!");
			byte var = buffer[++(*pos)];
			
			//splvar(var); spvar(value); 

			this->setVariableValue(var, value);

		}
	}

	// Advance to the next position (should put the pointer at the beginning of the next variable, if it exists)
	(*pos)++;
}


// Gets the value of a specific variable value
long ServiceProvider::getVariableValue(byte number)
{
	// Make sure the avriable exists
	if (number>=this->_max_variables) return 0;
	return (long)this->_variables[number]->read();
}


// Sets the value of a specific variable value
void ServiceProvider::setVariableValue(byte number, long value)
{
	//spf("setVariableValue  "); spvar(number); splvar(value);
	// Make sure the avriable exists
	if (number>=this->_max_variables) return;
	this->_variables[number]->write(value);
}


// Set all the devices to their default state
void ServiceProvider::resetDevices()
{
	//splf("resetDevices");
	for (byte i=0; i<this->_max_variables; i++) this->_variables[i]->reset();	

	this->writeOutputs();
}


// Read data from all the inputs
void ServiceProvider::readInputs()
{
	for (byte i=0; i<this->_max_variables; i++) this->_variables[i]->update();
}

// Write the new values to all the outputs
void ServiceProvider::writeOutputs()
{
	for (byte i=0; i<this->_max_variables; i++) this->_variables[i]->execute();
}


/*

// Returns a pointer to a specific variable, or NULL if the 
// variable with the given ID does not exist		
Variable* ServiceProvider::getDevice(byte id)
{
	// If the variable id is invalid, returns NULL
	if (id >= this->_max_variables || this->_variables[id] == NULL) 
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		splf("Invalid Variable request:"); spl(id);
		#endif
		return NULL;
	}
	return this->_variables[id];
}

// Returns a pointer to a specific variable only if the variable
// exists and matches the desired type	
Variable* ServiceProvider::getDevice(byte id, DEVICE_TYPE type)
{
	// Try to get the variable with the given id
	Variable* variable = this->getDevice(id);
	// If the variable exists, let's see if it's the right type
	if (variable != NULL)
	{
		// If it's not the right type, returns NULL
		if (variable->getDeviceType() != type)
		{
			#ifdef BEHAVIOUR_TREE_LOG_CREATION
			spf("Invalid Variable type request:"); spl(id);
			#endif
			return NULL;
		}
	}
	return variable;
}

*/
