#include "EEpromStream.h"


// Creates a new EEpromStream object, with a specific max size
EEpromStream::EEpromStream(unsigned int size)
{
	this->_size = size;
	this->_read_pointer = 0;
	this->_write_pointer = 0;
}


// Sets the read pointer to a specific position on the EEprom
void EEpromStream::setReadPointer(unsigned int pointer)
{
	this->_read_pointer = pointer;
}


// Sets the write pointer to a specific position on the EEprom
void EEpromStream::setWritePointer(unsigned int pointer)
{
	this->_write_pointer = pointer;
}


// Get the current position of the read pointer
unsigned int EEpromStream::getReadPointer()
{
	return this->_read_pointer;
}


// Get the current position of the write pointer
unsigned int EEpromStream::getWritePointer()
{
	return this->_write_pointer;
}


// Get the size of the EEprom
unsigned int EEpromStream::getSize()
{
	return this->_size;
}

// ------------------------------------------------------------------
// Methods to implement from the Stream class
// ------------------------------------------------------------------

// Returns the number of bytes from the current read position untill 
// the final EEprom position allocated to the stream
int EEpromStream::available()
{
	return this->_size - this->_read_pointer;
}


// Reads the next byte from the eeprom (or -1 if there are no more bytes to read)
int EEpromStream::read()
{
	if (this->_read_pointer >= this->_size) return -1;
	return EEPROM.read(this->_read_pointer++);
}


// Reads the next value of the stream, without moving to the next one
int EEpromStream::peek()
{
	if (this->_read_pointer >= this->_size) return -1;
	return EEPROM.read(this->_read_pointer);	
}


// This methos setd the read pointer to the end of the memory. read() will always
// return -1 untill we call setReadPointer with a valid memory position
void EEpromStream::flush()
{
	this->_read_pointer = this->_size;
}


// ------------------------------------------------------------------
// Methods to implement from the Print class
// ------------------------------------------------------------------
// Writes a byte to the next position on the eeprom
size_t EEpromStream::write(uint8_t value)
{
	eepromUpdate(&value);
	return 1;
}


// ------------------------------------------------------------------
// Methods to read and write specific types of data (in binary mode)
// because the print class only supports writting byte values
// ------------------------------------------------------------------


void EEpromStream::read(char *text)
{
	byte length = EEPROM.read(this->_read_pointer++);
	for (byte i=0; i<length; i++)
	{
		text[i] = EEPROM.read(this->_read_pointer++);	
	}
	text[length] = 0;
}


void EEpromStream::write(char *text)
{
	byte length = strlen(text);
	eepromUpdate( &length );
	for (byte i=0; i<length; i++)
	{
		eepromUpdate( (byte*)&text[i] );	
	}
	text[length] = 0;
}
	

void EEpromStream::read(byte *value)
{
	*value = EEPROM.read(this->_read_pointer++);
}


void EEpromStream::read(int *value)
{
	byte b1 = EEPROM.read(this->_read_pointer++);
	byte b0 = EEPROM.read(this->_read_pointer++);
	*value = (b1 << 8) + b0;
}


void EEpromStream::write(int *value)
{
	byte b0 = byte(*value);
	byte b1 = byte(*value >> 8);
	eepromUpdate(&b1);
	eepromUpdate(&b0);
}


void EEpromStream::read(long *value)
{
	byte b3 = EEPROM.read(this->_read_pointer++);
	byte b2 = EEPROM.read(this->_read_pointer++);
	byte b1 = EEPROM.read(this->_read_pointer++);
	byte b0 = EEPROM.read(this->_read_pointer++);
	*value = ((long)b3 << 24) + ((long)b2 << 16) + ((long)b1 << 8) + b0;	
}


void EEpromStream::write(long *value)
{
	byte b0 = byte(*value);
	byte b1 = byte(*value >> 8);
	byte b2 = byte(*value >> 16);
	byte b3 = byte(*value >> 24);
	eepromUpdate(&b3);
	eepromUpdate(&b2);
	eepromUpdate(&b1);
	eepromUpdate(&b0);
}


void EEpromStream::read(unsigned long *value)
{
	byte b3 = EEPROM.read(this->_read_pointer++);
	byte b2 = EEPROM.read(this->_read_pointer++);
	byte b1 = EEPROM.read(this->_read_pointer++);
	byte b0 = EEPROM.read(this->_read_pointer++);
	*value = ((unsigned long)b3 << 24) + ((unsigned long)b2 << 16) + ((unsigned long)b1 << 8) + b0;	
}


void EEpromStream::write(unsigned long *value)
{
	byte b0 = byte(*value);
	byte b1 = byte(*value >> 8);
	byte b2 = byte(*value >> 16);
	byte b3 = byte(*value >> 24);
	eepromUpdate(&b3);
	eepromUpdate(&b2);
	eepromUpdate(&b1);
	eepromUpdate(&b0);
}


// Updates the value of a specific position on the eeprom (only writes if it's necessary)
void EEpromStream::eepromUpdate( byte *value )
{
    byte aux = EEPROM.read(this->_write_pointer);
    if (aux != *value) EEPROM.write(this->_write_pointer, *value);
	this->_write_pointer++;
}
