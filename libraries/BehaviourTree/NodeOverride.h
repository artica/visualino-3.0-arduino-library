/*
Artica CC - http://artica.cc

Override node - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef ARTICA_TREE_NODE_OVERRIDE_H
#define ARTICA_TREE_NODE_OVERRIDE_H

#include "Arduino.h"
#include "NodeInternal.h"

#include "Utils.h"
#include "DebugConfig.h"

// Bit masks for the override policies on this node
// (the data is all stores in the _policies variable)
// The offset is the number of bits to shift for each mask
#define NODE_OVERRIDE_POLICY_FAILURE_MASK B00000011
#define NODE_OVERRIDE_POLICY_FAILURE_OFFSET 0
#define NODE_OVERRIDE_POLICY_RUNNING_MASK B00001100
#define NODE_OVERRIDE_POLICY_RUNNING_OFFSET 2
#define NODE_OVERRIDE_POLICY_SUCCESS_MASK B00110000
#define NODE_OVERRIDE_POLICY_SUCCESS_OFFSET 4

// Override from complete to runningcv

//   00 SUCCESS RUNNING FAILURE
// B 00   01      01      00 
	
// Tree node that has only one child node
// This node runs the child node and overrides the running
// state if necessary, based of a configurable override policy

class NodeOverride: public NodeInternal
{ 
	public:
		// This node type needs the id and a byte setting the override policies
		NodeOverride(byte id, byte policies);
		
		// Create a node based of a byte array of data. If the data size
		// is invalid to create the desired node, returns null
		static NodeOverride* createNode(byte* data, byte data_length);

		// Run the child node and override the result if necessary
		NODE_STATE run();

		// Stops the execution of the children node
		//NODE_STATE stop(NODE_STATE state);

		// Prints the node class name
		void printName() { spf("NodeOverride"); };
	
	private:
		// Prints a string with the name of the override policy for a specific state
		void printOverridePolicy(NODE_STATE state);

		// Byte array containing the override policy (2 bits necessary per state)
		byte _policies;
};

#endif
