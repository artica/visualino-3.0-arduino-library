/*
Artica CC - http://artica.cc

Generic node type - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "Node.h"

#include "Utils.h"
#include "DebugConfig.h"

// Default constructor
Node::Node(NODE_ID id)
{
	this->_id = id;
	this->_state = NODE_STATE_INACTIVE;
	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	spf("new Node:"); spx(id); spf("  ");
	#endif
}

// Destructor
Node::~Node()
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
	splf("~Node"); delay(DEBUG_DELAY);
	#endif
}

// Tests if a data length value is the exact number of bytes
// to create a node of a specific type
bool Node::testDataLength(byte data_length, byte data_necessary)
{
	// Make sure we have the right number of bytes for this node type
	if (data_length != data_necessary)
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		splf("Data length does not match the node type!");
		#endif
		return false;
	}
	return true;
}

// Runs the the node. Is the node wasn't running before, start() is called
// the parameter forces start() to be called even if the node was already running
NODE_STATE Node::run()
{
	if (this->_state != NODE_STATE_RUNNING) this->start();
	//spf("RUNNING NODE:"); spx(this->_id); sp(' '); printName(); spl();
	#ifdef BEHAVIOUR_TREE_DEBUG_DELAY_TIME
	delay(BEHAVIOUR_TREE_DEBUG_DELAY_TIME);
	#endif
	return NODE_STATE_RUNNING;	
}

// Starts the node execution
void Node::start()
{
	this->_state = NODE_STATE_RUNNING;
	#ifdef BEHAVIOUR_TREE_DEBUG_RUN
	spf("STARTING NODE:"); spx(this->_id); sp(' '); printName(); spl();
	#endif
	#ifdef BEHAVIOUR_TREE_LOG_RUN	
	spf(STRING_TREE_STATE ":"); spx(this->_id); sp(':'); spl(NODE_STATE_RUNNING);
	#endif
	#ifdef BEHAVIOUR_TREE_DEBUG_DELAY_TIME
	delay(BEHAVIOUR_TREE_DEBUG_DELAY_TIME);
	#endif
}

// Ends the node execution with a specific state
NODE_STATE Node::stop(NODE_STATE state)
{
	this->_state = state;
	#ifdef BEHAVIOUR_TREE_DEBUG_RUN
	spf("STOP RUNNING NODE:"); spx(this->_id); sp(' '); printName(); spf("  state:");
	switch(state)
	{
		//case NODE_STATE_RUNNING: splf("RUNNING"); break;
		case NODE_STATE_SUCCESS: splf("SUCCESS"); break;
		case NODE_STATE_FAILURE: splf("FAILURE"); break;
		case NODE_STATE_INACTIVE: splf("INACTIVE"); break;
	}
	#endif

	#ifdef BEHAVIOUR_TREE_LOG_RUN	
	spf(STRING_TREE_STATE ":"); spx(this->_id); sp(':'); spl(state);
	#endif
	#ifdef BEHAVIOUR_TREE_DEBUG_DELAY_TIME
	delay(BEHAVIOUR_TREE_DEBUG_DELAY_TIME);
	#endif
	return state;
}

// Prints a string with the name representing a node state
void Node::printStateName(NODE_STATE state)
{
	switch (state)
	{
		case NODE_STATE_FAILURE: spf("F"); break;
		case NODE_STATE_RUNNING: spf("R"); break;
		case NODE_STATE_SUCCESS: spf("S"); break;
	}
}


