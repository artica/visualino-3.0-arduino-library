/*
Artica CC - http://artica.cc

Test service support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/


#ifndef ARTICA_SERVICE_TEST_H
#define ARTICA_SERVICE_TEST_H

#include "Arduino.h"
#include "Utils.h"


#include "NodeService.h"

// Codes for the different test types

// Test if two values are equal
#define TEST_TYPE_EQUAL			0x00
// Test if two values are different
#define TEST_TYPE_DIFFERENT		0x01
// Test if a value is bigger than the other
#define TEST_TYPE_BIGGER		0x02
// Test if a value is smaller than the other
#define TEST_TYPE_SMALLER		0x03
// Test if a value is inside a range (defined by two other values)
#define TEST_TYPE_INSIDE		0x04
// Test if a value is outside a range (defined by two other values)
#define TEST_TYPE_OUTSIDE		0x05
// Test if a value has increased more than a threshhold since the last test
#define TEST_TYPE_INCREASE		0x06
// Test if a value has decreased more than a threshhold since the last test
#define TEST_TYPE_DECREASE		0x07
// Test if a value has changed more than a threshhold since the last test
#define TEST_TYPE_CHANGE		0x08


// Instances to use the delay service
class NodeServiceTest: public NodeService
{ 
	public:
		// A service instance node contains a node id and a pointer to the service provider
		// The data buffer contains different data depending on the node type
		NodeServiceTest( NODE_ID id, byte* data, byte data_length, ServiceProvider* service_provider );
		
		// Destructor
		~NodeServiceTest();

		// Create a node based of a byte array of data. If the data size
		// is invalid to create the desired node, returns null
		static NodeServiceTest* createNode(byte* data, byte data_length, ServiceProvider* service_provider);

		// Returns NODE_STATE_SUCCESS or NODE_STATE_FAILURE depending on
		// the evaluation of the chosen test and current values
		NODE_STATE run();
		
		// Starts the node execution, initializing all the necessary variables
		virtual void start();

		// Prints the node class name
		void printName() { spf("NodeServiceTest"); };	

		// Buffer for any tests that need persistent data between iterations
		byte* _persistent_data;

};

#endif
