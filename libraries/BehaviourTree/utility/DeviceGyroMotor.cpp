/*
Artica CC - http://artica.cc

Gyro Motor support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "DeviceGyroMotor.h"

#include "Utils.h"
#include "DebugConfig.h"


// Constructor of the device sets the pin to be used 
DeviceGyroMotor::DeviceGyroMotor( byte motor_position, int initial_value, ServiceProvider* service_provider ):
	DeviceGyro(service_provider),
	VariableInt(initial_value)
{
	this->_motor_position = motor_position;

	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	printName(); spl();
	#endif
	this->initialize();
}


// Initialize the device and obtain the initialization state
DEVICE_INITIALIZATION DeviceGyroMotor::initialize()
{
	// The Gyro motors are always initialized (in the secondary microcontroller)
	return DEVICE_INITIALIZATION_SUCCESSFUL;
}


// Create a device based of a byte array of data. If the data is invalid
// to create the desired device, returns null
DeviceGyroMotor* DeviceGyroMotor::createDevice(byte* data, byte data_length, ServiceProvider* service_provider)
{
	spf("DeviceGyroMotor:"); printArray(data, data_length);

	// Check if the data buffer has the exact amount of data to create the device
	if (!testDataLength(data_length, 5)) return NULL;

	// These 2 bytes should contain the 2 pin numbers necessary to control the motor
	byte motor_position = data[2];
	int initial_value = getInt16FromArray( data, 3);

	DeviceGyroMotor* device = new DeviceGyroMotor( motor_position, initial_value, service_provider );
	// Create the new device and return it
	return device;
}


// Update the the hardware pin with the current device value
void DeviceGyroMotor::execute()
{
	//spl(this->_value);
	
	#ifdef GYRO_ROBOT_SUPPORT
	if (this->_motor_position == GYRO_MOTOR_POSITION_LEFT)
		this->_service_provider->_gyro.motoruino2.setSpeedPWMLeft(this->_value);
	else if (this->_motor_position == GYRO_MOTOR_POSITION_RIGHT)
		this->_service_provider->_gyro.motoruino2.setSpeedPWMRight(this->_value);
		/**/
	#endif
}
