/*
Artica CC - http://artica.cc

Service node - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "NodeGyro.h"

#include "Utils.h"
#include "DebugConfig.h"


// A service instance node contains a node id and a pointer to the service provider
// The data buffer contains different data depending on the node type
NodeGyro::NodeGyro( NODE_ID id, byte* data, byte data_length, ServiceProvider* service_provider ): 
	Node(id)
{
	// Save the pointer to the service provider in use
	this->_service_provider = service_provider;
	// Copy the data buffer to a local node buffer
	this->_data = (byte*) malloc(sizeof(byte) * data_length);
	memcpy ( this->_data, data, data_length );

	#ifdef BEHAVIOUR_TREE_LOG_CREATION	
	printName(); spf("  data: "); printArray(data, data_length);
	#endif
}


// Destructor
NodeGyro::~NodeGyro()
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
	splf("~NodeGyro");
	#endif	
	free(this->_data);	
}

