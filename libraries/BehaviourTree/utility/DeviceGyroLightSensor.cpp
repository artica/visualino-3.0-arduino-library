/*
Artica CC - http://artica.cc

Gyro Light Sensor support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "DeviceGyroLightSensor.h"

#include "Utils.h"
#include "DebugConfig.h"


// Constructor of the device sets the pin to be used 
DeviceGyroLightSensor::DeviceGyroLightSensor( byte position, byte data_type, int threshold, ServiceProvider* service_provider):
	DeviceGyro(service_provider),
	VariableLong(0)
{
	this->_position = position;
	this->_data_type = data_type;
	this->_threshold = threshold;

	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	printName(); spl();
	#endif
	this->initialize();
}


// Initialize the device and obtain the initialization state
DEVICE_INITIALIZATION DeviceGyroLightSensor::initialize()
{
	// The Gyro motors are always initialized (in the secondary microcontroller)
	return DEVICE_INITIALIZATION_SUCCESSFUL;
}


// Create a device based of a byte array of data. If the data is invalid
// to create the desired device, returns null
DeviceGyroLightSensor* DeviceGyroLightSensor::createDevice(byte* data, byte data_length, ServiceProvider* service_provider)
{
	//spf("DeviceGyroLightSensor:"); printArray(data, data_length);

	// Check if the data buffer has the exact amount of data to create the device
	if (!testDataLength(data_length, 6)) return NULL;

	// These 4 bytes should contain the necessary data to initialize the devicecalc
	byte position = data[2];
	byte data_type = data[3];
	int threshold = getInt16FromArray( data, 4);

	DeviceGyroLightSensor* device = new DeviceGyroLightSensor( position, data_type, threshold, service_provider );
	// Create the new device and return it
	return device;
}


// Update the the hardware pin with the current device value
void DeviceGyroLightSensor::update()
{
	//spl(this->_value);

	#ifdef GYRO_ROBOT_SUPPORT

	//spf("PIMBA:");

	// Read the value from the sensor we want
	short unsigned int value = 0;
	switch(this->_position)
	{
		case GYRO_LIGHT_SENSOR_POSITION_LEFT:
			this->_service_provider->_gyro.bumpers.getLeftAmbient(&value);
			//spf("LEFT:"); spl(value);
		break;
		case GYRO_LIGHT_SENSOR_POSITION_RIGHT:
			this->_service_provider->_gyro.bumpers.getRightAmbient(&value);
			//spf("RIGHT:"); spl(value);
		break;
		case GYRO_LIGHT_SENSOR_POSITION_CENTER:
			this->_service_provider->_gyro.bumpers.getCenterAmbient(&value);
			//spf("LEFT:"); spl(value);
		break;
	}

	// Handle the data depending on the desired data type
	switch(this->_data_type)
	{
		// If we use analog data mode, copy the value directly
		case GYRO_LIGHT_SENSOR_DATA_MODE_ANALOG:
			this->_value = value;
		break;
		// If we use digital data mode, compare the value with the desired threshhold
		case GYRO_LIGHT_SENSOR_DATA_MODE_DIGITAL:
			this->_value = value >= this->_threshold;
		break;
	}

	//spf("value:"); spl(this->_value);

	#endif

}
