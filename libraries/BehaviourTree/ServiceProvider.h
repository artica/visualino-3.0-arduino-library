/*
Artica CC - http://artica.cc

Service Provider for a Tree - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef ARTICA_SERVICE_PROVIDER_H
#define ARTICA_SERVICE_PROVIDER_H

#include "Arduino.h"
#include "MessageConstants.h"
#include "Device.h"
#include "Variable.h"

#include "Utils.h"
#include "DebugConfig.h"


// Removing this line will disable the support for the Gyro robot, making the system
// use less memory, allowing the construction of bigger trees on normal arduinos
#define GYRO_ROBOT_SUPPORT


#ifdef GYRO_ROBOT_SUPPORT
	#include <Gyro.h>
	#include <Motoruino2.h>
	#include <GyroSerialCommand.h>
	#include <Wire.h>
	#include <Metro.h>
	#include <SPI.h>
	#include <Servo.h>

	// Define the direction for both motors (chose between 1 or -1)
	#define M1_SIGNAL -1
	#define M2_SIGNAL 1
#endif

// Set the data type used for service type identifiers
typedef byte SERVICE_TYPE;


// Main ServiceProvider class object
class ServiceProvider
{ 
	public:
		// Create the service provider
		ServiceProvider();
		// Destructor
		~ServiceProvider();
		
		// Initialize the ServiceProvider for a specific number of variables
		void init(byte max_variables);
		// Reset the ServiceProvider, freeing all allocated memory for variables
		void reset();

		// Adds a new variable to the service provider
		// data is binary encoded data used to initialize the variable
		// The first byte of data is the variable ID, the rest of the
		// data will depend on the variable type
		// Returns false if there is any error with the node data
		bool addVariable(byte* data, byte data_length);

		// Adds a new variable to the service provider
		// Returns false if the variable or variable id are invalid
		bool addVariable(VARIABLE_ID id, Variable* variable);

		// Get a numerical value from a data source
		// Data sources can be constants, variables or devices
		// The "pos" position of the buffer should be a header byte, 
		// identifying the data source and data type for the value
		long getValueFromBuffer(byte* buffer, byte* pos);


		// Sets a new value to a variable identified in a byte buffer
		void changeVariableValue(long value, byte* buffer, byte* pos);

		// Gets the value of a specific variable value
		long getVariableValue(byte number);

		// Sets the value of a specific variable value
		void setVariableValue(byte number, long value);


		// Set all the devices to their default state
		void resetDevices();
		/*
		// Returns a pointer to a specific variable, or NULL if the 
		// variable with the given ID does not exist		
		Device* getDevice(VARIABLE_ID id);
		
		// Returns a pointer to a specific variable only if the variable
		// exists and matches the desired type	
		Device* getDevice(VARIABLE_ID id, DEVICE_TYPE type);
		*/

	public:
		// Read data from all the inputs
		void readInputs();

		// Write the new values to all the outputs
		void writeOutputs();


		Variable** _variables;
		//byte _devices_length;
		byte _max_variables; 


	// Here we add the necessary variables to use the Gyro robot	
	#ifdef GYRO_ROBOT_SUPPORT
		Gyro _gyro;
	#endif
};

#endif