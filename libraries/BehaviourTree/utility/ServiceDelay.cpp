/*
Artica CC - http://artica.cc

Delay service support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "ServiceDelay.h"


// Constructor
NodeServiceDelay::NodeServiceDelay( NODE_ID id, byte* data, byte data_length, ServiceProvider* service_provider ):
	NodeService(id, data, data_length, service_provider)
{
	this->_start_time = 0;
}


// Destructor
NodeServiceDelay::~NodeServiceDelay()
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
	splf("~NodeServiceDelay");
	#endif
} 


// Create a node based on a byte array of data. If the data size
// is invalid to create the desired node, returns null
NodeServiceDelay* NodeServiceDelay:: createNode(byte* data, byte data_length, ServiceProvider* service_provider)
{
	// Test nodes have different data lenghts depending on the 
	// data sources and types involved in the test
	byte node_data_length = data[3];

	//splf("------------------NodeServiceDelay::createNode");
	//spl(data_length);
	//spl(node_data_length+4);

	// Check if the data buffer has the exact amount of data to create the node
	if (!testDataLength(data_length, node_data_length+4)) return NULL;


	// The first byte of the array contains the node id
	NODE_ID node_id = data[0];
	
	NodeServiceDelay* node = new NodeServiceDelay(node_id, &(data[4]), node_data_length, service_provider);
	// At this point everything should be fine, so let's create the node
	return node;
}


// Returns NODE_STATE_SUCCESS if enough time has passed since the 
// delay started, NODE_STATE_RUNNING otehrwise
NODE_STATE NodeServiceDelay::run()
{
	Node::run();

	byte pos = 0;

	// Get the duration for the delay from the service provider buffer
	// (the value can be a constant, or come from a variable or device)
	long duration = this->_service_provider->getValueFromBuffer(this->_data, &pos);

	//splvar(duration);

	// If it was working already, let's see how long has passed...
	// If enough time has passed, stop the delay and return success
	unsigned long elapsed_time = millis()-this->_start_time;
	if ( elapsed_time >= duration)
	{
		//spl(elapsed_time);
		this->stop(NODE_STATE_SUCCESS);
		return NODE_STATE_SUCCESS;
	}
	
	// If not enough time has passed, return running state
	return NODE_STATE_RUNNING;
}

// Set the start time of the delay as the current time
void NodeServiceDelay::start()
{
	Node::start();
	this->_start_time = millis();
}
