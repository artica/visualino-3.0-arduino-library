/*
Artica CC - http://artica.cc

Generic variable type - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "Variable.h"

#include "Utils.h"
#include "DebugConfig.h"

// Default constructor
Variable::Variable()
{
	#ifdef BEHAVIOUR_TREE_LOG_CREATION
	spf("new Variable:  "); //sp(id); spf("  ");
	#endif
}


// Destructor
Variable::~Variable()
{
	#ifdef BEHAVIOUR_TREE_DEBUG_CREATION
	splf("~Variable"); delay(DEBUG_DELAY);
	#endif
}


// Tests if a data length value is the exact number of bytes
// necessary to create a variable of a specific type
bool Variable::testDataLength(byte data_length, byte data_necessary)
{
	// Make sure we have the right number of bytes for this variable type
	if (data_length != data_necessary)
	{
		#ifdef BEHAVIOUR_TREE_LOG_CREATION
		splf("Data length does not match the variable type!");
		#endif
		return false;
	}
	return true;
}


