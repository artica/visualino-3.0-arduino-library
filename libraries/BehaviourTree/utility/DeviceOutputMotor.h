/*
Artica CC - http://artica.cc

Motor support - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef ARTICA_DEVICE_OUTPUT_MOTOR_H
#define ARTICA_DEVICE_OUTPUT_MOTOR_H

#include "Arduino.h"
#include "Device.h"
#include "VariableInt.h"

#include "Utils.h"
#include "DebugConfig.h"

// Device for a differential motor output
class DeviceOutputMotor:public Device, public VariableInt
{ 
	public:
		// The constructor of the device sets the pins to be used for both motors
		DeviceOutputMotor( byte pin_1, byte pin_2, int initial_value);
		
		// Initialize the device and obtain the initialization state
		DEVICE_INITIALIZATION initialize();

		// Returns the device type for this class
		DEVICE_TYPE getType() {	return VARIABLE_OUTPUT_MOTOR; };
		
		// Create a device based on a byte array of data. If the data size 
		// is invalid to create the desired device, returns null
		static DeviceOutputMotor* createDevice(byte* data, byte data_length);

		// Prints the device class name
		void printName() { spf("DeviceOutputMotor"); };
	
		// Update the the hardware pin with the current device value
		void execute();

	// @todo por isto protected depois das mudanças	
	//protected:

		// Pins used for the motor
		byte _pin_1;
		byte _pin_2;
};

#endif
