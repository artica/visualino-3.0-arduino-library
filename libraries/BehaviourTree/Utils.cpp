// Small generic utility functions
#include "Utils.h"


// Convert a hexadecimal character to a numerical value
byte hex2byte(char hex)
{
	if (hex>='0' && hex<='9') return hex-'0'; 
	if (hex>='A' && hex<='F') return hex-'A'+10; 
	if (hex>='a' && hex<='f') return hex-'a'+10;
	return 0;
}

// Parse two chars of a byte array as a hexadecimal number
byte parseHexByte(char* buffer, byte pos)
{
	return hex2byte(buffer[pos])*16 + hex2byte(buffer[pos+1]);
}

// Parse eight chars of a byte array as a hexadecimal number
unsigned long parseHexULong(char* buffer, byte pos)
{
	splf("parseHexULong");
	spl(parseHexByte( buffer, pos));
	spl(parseHexByte( buffer, pos+2));
	spl(parseHexByte( buffer, pos+4));
	spl(parseHexByte( buffer, pos+6));
	return (unsigned long)parseHexByte( buffer, pos) << 24 |
							parseHexByte( buffer, pos+2) << 16 | 
							parseHexByte( buffer, pos+4) << 8 | 
							parseHexByte( buffer, pos+6) << 24;
}

// Prints an array of byte values
void printArray(byte* array, byte length)
{
	spf("array:");
	for (byte i=0; i<length; i++)
	{
		sp(array[i]); sp(' ');
	}
	spl();
}


// Extract an int variable from a specific position in a byte array
int getIntFromArray(byte* data, byte pos)
{
    	int value =   (int)data[pos]<<8 | 
    		      (int)data[pos+1];
    return value;
}

// Extract an unsigned long variable from a specific position in a byte printArray
unsigned long getUnsignedLongFromArray(byte* data, byte pos)
{
	unsigned long value =	(unsigned long)data[pos]<<24 | 
							(unsigned long)data[pos+1]<<16 | 
							(unsigned long)data[pos+2]<<8 | 
							(unsigned long)data[pos+3];
    return value;
}


// Global variable necessary for the debug timer
unsigned long _my_debug_timer;

// Start a timer to measure the time (in microseconds);
void startTimer()
{
	_my_debug_timer = micros();
}


// Print how much time has passed since startTimer() was called
void checkTimer()
{
	long end_time = micros();
	spf("Timer(us):"); spl(end_time - _my_debug_timer);

}




