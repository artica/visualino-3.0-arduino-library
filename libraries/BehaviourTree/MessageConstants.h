/*
Artica CC - http://artica.cc

Global Constants  - Tarquinio Mota

V1.0 - 23/11/2015


This Software is under The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#ifndef ARTICA_MESSAGE_CONSTANTS_H
#define ARTICA_MESSAGE_CONSTANTS_H

// Constants used in the communication between the interface and the Arduino


// Possible data sources types
#define DATA_SOURCE_TYPE_CONSTANT		0x00
#define DATA_SOURCE_TYPE_VARIABLE		0x01
#define DATA_SOURCE_TYPE_DEVICE			0x02

// Codes for the different variable types
#define VARIABLE_DATA_BOOL				0x00
#define VARIABLE_DATA_BYTE				0x01
#define VARIABLE_DATA_INT				0x02
#define VARIABLE_DATA_LONG				0x03


// Codes for the different input/output types
#define VARIABLE_OUTPUT_DIGITAL_PIN				0x10
#define VARIABLE_OUTPUT_ANALOG_PIN				0x11
#define VARIABLE_OUTPUT_MOTOR					0x12

#define VARIABLE_INPUT_DIGITAL_PIN				0x20
#define VARIABLE_INPUT_ANALOG_PIN				0x21

#define VARIABLE_GYRO_OUTPUT_MOTOR				0x30
#define VARIABLE_GYRO_INPUT_DISTANCE_SENSOR		0x40
#define VARIABLE_GYRO_INPUT_LIGHT_SENSOR		0x41


// Codes for the different node service types
#define SERVICE_DELAY 					0x00
#define SERVICE_TEST					0x01
#define SERVICE_FUNCTION	 			0x02

// String used to send the Tree State messages
#define STRING_TREE_STATE				"TS"

// String used to send the Variable state messages
#define STRING_VARIABLES_STATE			"VS"

// Separator symbol for the message parameters
#define STRING_MESSAGE_SEPARATOR		","
#define PARAMETER_MESSAGE_SEPARATOR		";"


#endif